**In this class several topics from Machine Learning were explored theoretically and in practice**

**There are 4 main modules in the class, each one implements a project with real data focused on a different part of ML**:

1. Supervised Learning
2. Unsupervised Learning
3. Randomized Optimization
4. Reinforcement Learning