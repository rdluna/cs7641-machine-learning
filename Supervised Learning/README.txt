The algorithms are all implemented with Matlab, the SVM training library libsvm requires a Windows environment.
This requires Matlab r2017a, provided by GT's academic licenses (http://matlab.gatech.edu/)
It can be used in one of the remote desktops from the VLab (https://mycloud.gatech.edu/), 
although it's best to run locally.
There are 2 folders, one for each dataset (Adult and TicTacToe)
The datasets are already contained in each folder.
Each folder contains the following codes:
- train_all: just calls all the individual training scripts
- train_tree: implementation of decision trees
- train_nnet: implementation of neural networks
- train_knn: implementation of knn
- train_all_svm: implementation of SVM using libsvm, in this case the subfunctions and outputs
 are located in the libsvm/windows directory, the libsvm binaries are used, so Windows must be used.
- train_boosting: implementation of boosting
For the Adult dataset the whole training will take several hours, the SVM alone with the fine and 
coarse grid search can take more than 8 hours to run.
For the TicTacToe dataset everything runs in manageable time.
The training of the neural networks uses the paralleling computing toolbox, 
it may take a while for the pool to initialize locally before the training of the nets begins.
