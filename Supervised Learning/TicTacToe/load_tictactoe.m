%% Import data from text file.
% Script for importing data from the following text file:
%
%    C:\Users\rodri\Documents\MATLAB\Cancer\tic-tac-toe.data
%
% To extend the code to different selected data or a different text file,
% generate a function instead of a script.

% Auto-generated by MATLAB on 2017/09/14 20:06:27

%% Initialize variables.
filename = 'C:\Users\rodri\Documents\MATLAB\TicTacToe\tic-tac-toe.data';
delimiter = ',';

%% Format for each line of text:
%   column1: categorical (%C)
%	column2: categorical (%C)
%   column3: categorical (%C)
%	column4: categorical (%C)
%   column5: categorical (%C)
%	column6: categorical (%C)
%   column7: categorical (%C)
%	column8: categorical (%C)
%   column9: categorical (%C)
%	column10: categorical (%C)
% For more information, see the TEXTSCAN documentation.
formatSpec = '%C%C%C%C%C%C%C%C%C%C%[^\n\r]';

%% Open the text file.
fileID = fopen(filename,'r');

%% Read columns of data according to the format.
% This call is based on the structure of the file used to generate this
% code. If an error occurs for a different file, try regenerating the code
% from the Import Tool.
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'TextType', 'string',  'ReturnOnError', false);

%% Close the text file.
fclose(fileID);

%% Post processing for unimportable data.
% No unimportable data rules were applied during the import, so no post
% processing code is included. To generate code which works for
% unimportable data, select unimportable cells in a file and regenerate the
% script.

%% Create output variable
data = table(dataArray{1:end-1}, 'VariableNames', {'b11','b12','b13','b21','b22','b23','b31','b32','b33','class'});
data.class = grp2idx(data.class) - 1;
%% Clear temporary variables
clearvars filename delimiter formatSpec fileID dataArray ans i v;