% Decision Trees
train_tree
% Neural Networks
train_nnet
% kNN
train_knn
% SVM with LIBSVM, all outputs are in libsvm/windows
train_all_svm
% Boosting
train_boost
fclose('all');