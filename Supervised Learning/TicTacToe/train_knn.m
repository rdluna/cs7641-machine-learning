clc
clear
close force all
load_tictactoe

v = data.Properties.VariableNames(1:end-1);
for i = 1:numel(v)
    eval(['data.' v{i} '=grp2idx(data.' v{i} ');']);
end

rng(0112358)
[train_idx,test_idx] = dividerand(size(data,1),0.70,0.30);
data_train = data(train_idx,:);
data_test = data(test_idx,:);

train_split = 0.90:-0.10:0.10;
test_split  = 0.10:0.10:0.90;

cdir = pwd;
cd('knn')

%% kNN
data_train_knn = data_train;
data_test_knn = data_test;
knn_n = 2:2:100;

knn_euclidean = cell(numel(knn_n),1);
knn_crossval_err_euclidean = zeros(numel(knn_n),1);
knn_test_perf_euclidean = cell(numel(knn_n),1);
knn_test_err_euclidean = zeros(numel(knn_n),1);
knn_cityblock = cell(numel(knn_n),1);
knn_crossval_err_cityblock = zeros(numel(knn_n),1);
knn_test_perf_cityblock = cell(numel(knn_n),1);
knn_test_err_cityblock = zeros(numel(knn_n),1);
knn_euclidean_time = zeros(numel(knn_n),1);
knn_cityblock_time = zeros(numel(knn_n),1);

for i = 1:numel(knn_n)
    fprintf('Running for K = %s\n',num2str(knn_n(i)));
    tic
    knn_euclidean{i} = fitcknn(data_train_knn,'class','Distance','euclidean','NumNeighbors',knn_n(i),'Standardize',1);
    knn_euclidean_time(i) = toc;
    knn_crossval_err_euclidean(i) = knn_euclidean{i}.crossval.kfoldLoss;
    knn_test_perf_euclidean{i} = classperf(data_test_knn.class,knn_euclidean{i}.predict(data_test_knn));
    knn_test_err_euclidean(i) = knn_test_perf_euclidean{i}.ErrorRate;
    tic
    knn_cityblock{i} = fitcknn(data_train_knn,'class','Distance','cityblock','NumNeighbors',knn_n(i),'Standardize',1);
    knn_cityblock_time(i) = toc;
    knn_crossval_err_cityblock(i) = knn_cityblock{i}.crossval.kfoldLoss;
    knn_test_perf_cityblock{i} = classperf(data_test_knn.class,knn_cityblock{i}.predict(data_test_knn));
    knn_test_err_cityblock(i) = knn_test_perf_cityblock{i}.ErrorRate;
end

knn_euclidean_table = sortrows([knn_crossval_err_euclidean knn_n']);
knn_cityblock_table = sortrows([knn_crossval_err_cityblock knn_n']);

figure
hold on
grid on
box on
h1 = plot(knn_n,knn_crossval_err_euclidean,'-kd');
scatter(knn_euclidean_table(1,2),knn_euclidean_table(1,1),'gd','filled')
h2 = plot(knn_n,knn_crossval_err_cityblock,'-bd');
scatter(knn_cityblock_table(1,2),knn_cityblock_table(1,1),'gd','filled')
xlabel('Number of Neighbors')
ylabel('Crossvalidation error')
legend([h1,h2],{'Euclidean distance','Cityblock distance'})
set(gcf,'Color','w')
saveas(gcf,'knn_distances.png')

%% KNN Learning Curve
if knn_cityblock_table(1,1) < knn_euclidean_table(1,1)
    best_distance = 'cityblock';
    best_k = knn_cityblock_table(1,2);
else
    best_distance = 'euclidean';
    best_k = knn_euclidean_table(1,2);
end

lc_knn_cv_error = zeros(numel(train_split),1);
lc_knn_test_error = zeros(numel(train_split),1);

for ii = 1:numel(train_split)
    [lc_train_idx,lc_test_idx] = dividerand(size(data,1),train_split(ii),test_split(ii));
    lc_train = data(lc_train_idx,:);
    lc_test = data(lc_test_idx,:);
    lc_knn = fitcknn(lc_train,'class','Distance',best_distance,'NumNeighbors',best_k,'Standardize',1);
    lc_knn_cv = crossval(lc_knn,'KFold',5);
    lc_knn_cv_error(ii) = lc_knn_cv.kfoldLoss;
    lc_knn_test = lc_knn.predict(lc_test);
    lc_knn_test_perf = classperf(lc_test.class,lc_knn_test);
    lc_knn_test_error(ii) = lc_knn_test_perf.ErrorRate;
end

save('lc_knn_cv_error','lc_knn_cv_error')
save('lc_knn_test_error','lc_knn_test_error')

figure
hold on
plot(train_split,lc_knn_cv_error,'-kd')
plot(train_split,lc_knn_test_error,'-bd')
grid on
box on
legend('Crossvalidation error','Test error')
xlabel('Training split')
set(gcf,'Color','w')
saveas(gcf,'knn_lc.png');

%% Confusion matrices

start_knn = fitcknn(data_train_knn,'class','Distance','euclidean','Standardize',1);
best_knn = fitcknn(data_train_knn,'class','Distance',best_distance,'NumNeighbors',best_k,'Standardize',1);

plotconfusion(data_train_knn.class',best_knn.predict(data_train_knn)')
saveas(gcf,'knn_best_confusion.png')
plotconfusion(data_test_knn.class',best_knn.predict(data_test_knn)')
saveas(gcf,'knn_best_confusion_t.png')

plotconfusion(data_train_knn.class',start_knn.predict(data_train_knn)')
saveas(gcf,'knn_start_confusion.png')
plotconfusion(data_test_knn.class',start_knn.predict(data_test_knn)')
saveas(gcf,'knn_start_confusion_t.png')

cd(cdir)