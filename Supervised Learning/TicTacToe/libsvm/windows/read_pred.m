function labels = read_pred(file)
    fileID = fopen(file);
    data = textscan(fileID,'%f');
    labels = data{1};
end