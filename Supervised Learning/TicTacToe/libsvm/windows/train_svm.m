function [status, cmdout] = train_svm(svm_type,kernel_type,fname,varargin)
if nargin > 3
    options = varargin{1};
end

cmd_str = {'svm-train '};

switch svm_type
    case 'C-SVC'
        s = 0;
    case 'nu-SVC'
        s = 1;
    case 'one-class'
        s = 2;
    case 'epsilon-SVR'
        s = 3;
    case 'nu-SVR'
        s = 4;
end
cmd_str = strcat(cmd_str,{'-s '},num2str(s),{' '});

switch kernel_type
    case 'linear'
        t = 0;
    case 'polynomial'
        t = 1;
    case 'radial'
        t = 2;
    case 'sigmoid'
        t = 3;
end
cmd_str = strcat(cmd_str,{'-t '},num2str(t),{' '});

% degree
d = 3;
if nargin > 3
    if isfield(options,'degree')
        d = options.degree;
    end
    cmd_str = strcat(cmd_str,{'-d '},num2str(d),{' '});
end

% gamma
g = 3;
if nargin > 3
    if isfield(options,'gamma')
        g = options.gamma;
    end
    cmd_str = strcat(cmd_str,{'-g '},num2str(g),{' '});
end

% coef0
r = 0;
if nargin > 3
    if isfield(options,'coef0')
        r = options.coef0;
    end
    cmd_str = strcat(cmd_str,{'-r '},num2str(r),{' '});
end

% cost
c = 1;
if nargin > 3 && (s == 0 || s == 3 || s == 4)
    if isfield(options,'cost')
        c = options.cost;
    end
    cmd_str = strcat(cmd_str,{'-c '},num2str(c),{' '});
end

% nu
n = 0.5;
if nargin > 3 && (s == 1 || s == 2 || s == 4)
    if isfield(options,'nu')
        n = options.nu;
    end
    cmd_str = strcat(cmd_str,{'-n '},num2str(n),{' '});
end

% epsilon
p = 0.1;
if nargin > 3 && s == 3
    if isfield(options,'loss_epsilon')
        p = options.loss_epsilon;
    end
    cmd_str = strcat(cmd_str,{'-p '},num2str(p),{' '});
end

% epsilon
e = 0.001;
if nargin > 3
    if isfield(options,'epsilon')
        e = options.epsilon;
    end
    cmd_str = strcat(cmd_str,{'-e '},num2str(e),{' '});
end

% epsilon
h = 1;
if nargin > 3
    if isfield(options,'shrinking')
       h = options.shrinking;
    end
    cmd_str = strcat(cmd_str,{'-h '},num2str(h),{' '});
end

% probability_estimates
b = 0;
if nargin > 3 && (s == 0 || s == 1 || s == 3 || s == 4) 
    if isfield(options,'probability_estimates')
        b = options.probability_estimates;
    end
    cmd_str = strcat(cmd_str,{'-b '},num2str(b),{' '});
end

% weight
wi = 1;
if nargin > 3 && s == 0
    if isfield(options,'wi')
        wi = options.wi;
    end
    cmd_str = strcat(cmd_str,{'-wi '},num2str(wi),{' '});
end

%crossval
v = 10;
if nargin > 3
    if isfield(options,'crossval')
        v = options.crossval;
    end
    if v > 0
        cmd_str = strcat(cmd_str,{'-v '},num2str(v),{' '});
    end
end

cmd_str = strcat(cmd_str,fname);
disp(cmd_str);
[status, cmdout] = system(cmd_str{1});

end