function [status, cmdout] = pred_svm(test_file,model_file,output_file)
    cmd_str = strcat({'svm-predict '},test_file,{' '},model_file,{' '},output_file);
    disp(cmd_str);
    [status, cmdout] = system(cmd_str{1});
end
