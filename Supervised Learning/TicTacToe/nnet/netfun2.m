function testval = netfun2(XTRAIN, YTRAIN, XTEST, YTEST, train_split, test_split, val_split, lr)

net = patternnet(10,'traingd');
net.trainParam.showWindow = 0;
net.trainParam.showCommandLine = 1;
net.trainParam.epochs = 1000;
net.trainParam.lr = lr;
net.divideParam.trainRatio = train_split;
net.divideParam.testRatio = test_split;
net.divideParam.valRatio = val_split;

net = train(net, XTRAIN', YTRAIN','useParallel','yes');
yNet = net(XTEST');
classNet = (yNet(1,:) > yNet(2,:))';
classTest = YTEST(:,1);
cp = classperf(classTest, classNet);
testval = cp.ErrorRate;

end