function testval = netfun(XTRAIN, YTRAIN, XTEST, YTEST, n, l, e, lr, trainfn)

net = patternnet(repmat(n,1,l),trainfn);
net.trainParam.showWindow = 0;
net.trainParam.showCommandLine = 1;
net.trainParam.epochs = e;
net.trainParam.lr = lr;
net.divideParam.trainRatio = 0.80;
net.divideParam.testRatio = 0.10;
net.divideParam.valRatio = 0.10;

net = train(net, XTRAIN', YTRAIN','useParallel','yes');
yNet = net(XTEST');
classNet = (yNet(1,:) > yNet(2,:))';
classTest = YTEST(:,1);
cp = classperf(classTest, classNet);
testval = cp.ErrorRate;

end