clc
clear
close force all
load_tictactoe

rng(0112358)
[train_idx,test_idx] = dividerand(size(data,1),0.70,0.30);
data_train = data(train_idx,:);
data_test = data(test_idx,:);

train_split = 0.90:-0.10:0.10;
test_split  = 0.10:0.10:0.90;

cdir = pwd;
cd('nnet')
%% Neural networks - Part 0
data_train_nn = data_train;
data_test_nn = data_test;

v = data_train_nn.Properties.VariableNames(1:end-1);
for i = 1:size(data_train_nn,2)-1
    eval(['data_train_nn.' v{i} '=grp2idx(data_train_nn.' v{i} ');'])
    eval(['data_test_nn.' v{i} '=grp2idx(data_test_nn.' v{i} ');'])
end

inputs_train_nn = table2array(data_train_nn(:,1:end-1));
output_train_nn = table2array(data_train_nn(:,end));
inputs_test_nn = table2array(data_test_nn(:,1:end-1));
output_test_nn = table2array(data_test_nn(:,end));
output_train_nn(:,2) = ~output_train_nn;
output_test_nn(:,2) = ~output_test_nn;

%% Neural networks - Part 1
n_neurons = 10;
n_layers = 1;
learning_rate = linspace(0.01,0.9,10);

% Training a classification network with standard gradient descent by
% optimizing learning rate
net_crossv_error = zeros(numel(n_layers),numel(n_neurons));
net_time1 = zeros(numel(n_layers),numel(n_neurons));

for j = 1:numel(learning_rate)
    tic
    net_crossv_error(j) = mean(crossval(@(XTRAIN, YTRAIN, XTEST, YTEST)netfun(XTRAIN, YTRAIN, XTEST, YTEST, n_neurons, n_layers, 1000, learning_rate(j), 'traingd'),...
        inputs_train_nn, output_train_nn, 'KFold',5));
    net_time1(j) = toc;
end
[~,net_min_cv] = min(net_crossv_error);
net_best_lr = learning_rate(net_min_cv);

save('net_crossv_error','net_crossv_error')
save('net_time1','net_time1')

figure
clf
plot(learning_rate,net_crossv_error,'-kd')
grid on
box on
xlabel('Gradient descent learning rate')
ylabel('Crossvalidation error')
set(gcf,'Color','w')
saveas(gcf,'nnet_train1.png')

%% Neural networks - Part 2
% Check if changing training/validation/test splits affects the
% crossvalidation error
net_crossv_error_split = zeros(numel(n_layers),numel(n_neurons));
net_time2 = zeros(numel(n_layers),numel(n_neurons));
rng(1123)
net_splits = rand(20, 3);
rowsum = sum(net_splits,2);
net_splits = bsxfun(@rdivide, net_splits, rowsum);

for i = 1:size(net_splits,1)
    tic
    net_crossv_error_split(i) = mean(crossval(@(XTRAIN, YTRAIN, XTEST, YTEST)netfun2(XTRAIN, YTRAIN, XTEST, YTEST, net_splits(i,1), net_splits(i,2), net_splits(i,3), net_best_lr),...
        inputs_train_nn, output_train_nn, 'KFold',5));
    net_time2(i) = toc;
end

save('net_crossv_error_split','net_crossv_error_split')
save('net_time2','net_time2')

net_splits_table = array2table(sortrows([net_crossv_error_split' net_splits]),'VariableNames',{'train','test','validation','cverror'});

figure
plot(net_crossv_error_split,'-kd')
xlim([1 10])
grid on
box on
xlabel('Split')
ylabel('Crossvalidation error')
set(gcf,'Color','w')
saveas(gcf,'nnet_train2.png')

figure
subplot(3,1,1)
scatter(net_splits(:,1),net_crossv_error_split,'kd')
xlim([0 1])
xlabel('Training split')
subplot(3,1,2)
scatter(net_splits(:,2),net_crossv_error_split,'kd')
xlim([0 1])
xlabel('Test split')
ylabel('Crossvalidation error')
subplot(3,1,3)
scatter(net_splits(:,3),net_crossv_error_split,'kd')
xlim([0 1])
xlabel('Validation split')
set(gcf,'Color','w')
saveas(gcf,'nnet_train3.png');

%% Neural Network Learning Curve
lc_net_cv_error = zeros(numel(train_split),1);
lc_net_test_error = zeros(numel(train_split),1);
lc_net_perf = cell(numel(train_split),1);

for ii = 1:numel(train_split)
    [lc_train_idx,lc_test_idx] = dividerand(size(data,1),train_split(ii),test_split(ii));
    lc_train = data(lc_train_idx,:);
    lc_test = data(lc_test_idx,:);
    for i = 1:size(data_train_nn,2)-1
        eval(['lc_train.' v{i} '=grp2idx(lc_train.' v{i} ');'])
        eval(['lc_test.' v{i} '=grp2idx(lc_test.' v{i} ');'])
    end
    inputs_train_lc = table2array(lc_train(:,1:end-1));
    output_train_lc = table2array(lc_train(:,end));
    inputs_test_lc = table2array(lc_test(:,1:end-1));
    output_test_lc = table2array(lc_test(:,end));
    output_train_lc(:,2) = ~output_train_lc;
    output_test_lc(:,2) = ~output_test_lc;
    lc_net_cv_error(ii) = mean(crossval(@(XTRAIN, YTRAIN, XTEST, YTEST)netfun2(XTRAIN, YTRAIN, XTEST, YTEST, net_splits_table{1,2}, net_splits_table{1,3}, net_splits_table{1,4}, net_best_lr),...
        inputs_train_lc, output_train_lc, 'KFold',5));
    lc_net_test_error(ii) = netfun2(inputs_train_lc, output_train_lc, inputs_test_lc, output_test_lc, net_splits_table{1,2}, net_splits_table{1,3}, net_splits_table{1,4}, net_best_lr);
end

save('lc_net_cv_error','lc_net_cv_error')
save('lc_net_test_error','lc_net_test_error')

figure
hold on
plot(train_split,lc_net_cv_error,'-kd')
plot(train_split,lc_net_test_error,'-bd')
grid on
box on
legend('Crossvalidation error','Test error')
xlabel('Training split')
set(gcf,'Color','w')
saveas(gcf,'nnet_lc.png');

%% Neural Networks Confusion Matrices
net_default = patternnet(10,'traingd');
net_default.trainParam.showWindow = 0;
net_default = train(net_default,inputs_train_nn',output_train_nn','useParallel','yes');
net_default_train = net_default(inputs_train_nn');
net_default_train = (net_default_train(1,:) > net_default_train(2,:))';
net_default_test = net_default(inputs_test_nn');
net_default_test = (net_default_test(1,:) > net_default_test(2,:))';

plotconfusion(output_train_nn(:,1)',net_default_train')
saveas(gcf,'net_default_confusion.png')
plotconfusion(output_test_nn(:,1)',net_default_test')
saveas(gcf,'net_default_confusion_t.png')

net_best = patternnet(10,'traingd');
net_best.trainParam.showWindow = 0;
net_best.trainParam.showCommandLine = 1;
net_best.trainParam.epochs = 1000;
net_best.trainParam.lr = net_best_lr;
net_best.divideParam.trainRatio = net_splits_table{1,2};
net_best.divideParam.testRatio = net_splits_table{1,3};
net_best.divideParam.valRatio = net_splits_table{1,4};
net_best = train(net_best,inputs_train_nn',output_train_nn','useParallel','yes');
net_best_train = net_best(inputs_train_nn');
net_best_train = (net_best_train(1,:) > net_best_train(2,:))';
net_best_test = net_best(inputs_test_nn');
net_best_test = (net_best_test(1,:) > net_best_test(2,:))';

plotconfusion(output_train_nn(:,1)',net_best_train')
saveas(gcf,'net_best_confusion.png')
plotconfusion(output_test_nn(:,1)',net_best_test')
saveas(gcf,'net_best_confusion_t.png')

cd(cdir)