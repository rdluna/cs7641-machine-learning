clc
clear
close force all
load_tictactoe

rng(0112358)
[train_idx,test_idx] = dividerand(size(data,1),0.70,0.30);
data_train = data(train_idx,:);
data_test = data(test_idx,:);

train_split = 0.90:-0.10:0.10;
test_split  = 0.10:0.10:0.90;

cdir = pwd;
cd('tree')

%% Classification trees
% Fit a full-depth tree with automatic pruning
tree = fitctree(data_train,'class','MergeLeaves','off','Prune','on');

% Initialize outputs for number of nodes and errors per pruning level
prune_levels = 0:(max(tree.PruneList) - 1);
pruned_tree = cell(numel(prune_levels),0);
num_nodes = zeros(numel(prune_levels),0);

% Get number of nodes and errors for each pruning level
for i = 1:numel(prune_levels)
    pruned_tree{i} = tree.prune('level',prune_levels(i));
    num_nodes(i) = pruned_tree{i}.NumNodes;
end

% Set seed for repeatability
rng(666)
% Calculate 5-fold crossvalidation error for the whole tree at each pruning level
[tree_crossv_err,~,~,best_level] = cvloss(tree,'SubTrees',0:max(tree.PruneList) - 1,'KFold',5);
% Select the tree with the lowest crossvalidation error
best_tree = prune(tree,'level',best_level);

save('tree_crossv_err','tree_crossv_err')
save('decision_tree','best_tree')

% Plot the errors
figure
clf
hold on
box on
grid on
plot(num_nodes,tree_crossv_err,'-kd')
xlabel('Number of nodes')
ylabel('Crossvalidation error')
hAx = gca;
hAx.XDir = 'reverse';
set(gcf,'Color','w')
saveas(gcf,'tree_error.png')

plotconfusion(data_train.class',best_tree.predict(data_train)')
saveas(gcf,'tree_pruned_confusion.png')
plotconfusion(data_train.class',tree.predict(data_train)')
saveas(gcf,'tree_unpruned_confusion.png')

plotconfusion(data_test.class',best_tree.predict(data_test)')
saveas(gcf,'tree_pruned_confusion_t.png')
plotconfusion(data_test.class',tree.predict(data_test)')
saveas(gcf,'tree_unpruned_confusion_t.png')

view(tree,'mode','Graph')
view(best_tree,'mode','Graph')

lc_tree_cv_error = zeros(numel(train_split),1);
lc_tree_test_error = zeros(numel(train_split),1);
lc_test_perf = cell(numel(train_split),1);

for ii = 1:numel(train_split)
    [lc_train_idx,lc_test_idx] = dividerand(size(data,1),train_split(ii),test_split(ii));
    lc_train = data(lc_train_idx,:);
    lc_test = data(lc_test_idx,:);
    lc_tree = fitctree(lc_train,'class','MergeLeaves','off','Prune','on');
    % Calculate 5-fold crossvalidation error for the whole tree at each pruning level
    [lc_tree_crossv_err,~,~,lc_best_level] = cvloss(lc_tree,'SubTrees',0:max(lc_tree.PruneList) - 1,'KFold',5);
    lc_tree_cv_error(ii) = lc_tree_crossv_err(lc_best_level);
    % Select the tree with the lowest crossvalidation error
    lc_best_tree = prune(lc_tree,'level',lc_best_level);
    lc_test_perf{ii} = classperf(lc_test.class,lc_best_tree.predict(lc_test));
    lc_tree_test_error(ii) = lc_test_perf{ii}.ErrorRate;
end

save('lc_tree_cv_error','lc_tree_cv_error')
save('lc_tree_test_error','lc_tree_test_error')

figure
hold on
plot(train_split,lc_tree_cv_error,'-kd')
plot(train_split,lc_tree_test_error,'-bd')
grid on
box on
legend('Crossvalidation error','Test error')
xlabel('Training split')
set(gcf,'Color','w')
saveas(gcf,'tree_lc.png');

cd(cdir)