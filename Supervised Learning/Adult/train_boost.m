clc
clear
close force all
load_train
load_test
data = vertcat(adult_train,adult_test);

rng(0112358)
[train_idx,test_idx] = dividerand(size(data,1),0.70,0.30);
data_train = data(train_idx,:);
data_test = data(test_idx,:);

train_split = 0.90:-0.10:0.10;
test_split  = 0.10:0.10:0.90;

cdir = pwd;
cd('boosting')

%% Boosting
ensemble_adaboost_lr = [linspace(0.001,0.1,10) 0.2:0.1:1];
ensemble_adaboost_crossval_err = zeros(numel(ensemble_adaboost_lr),1);
ensemble_adaboost_time = zeros(numel(ensemble_adaboost_lr),1);

for i = 1:numel(ensemble_adaboost_lr)
    tic
    boosted_tree = fitcensemble(data_train,'K','Method','AdaBoostM1','Learners','tree','CrossVal','on','LearnRate',ensemble_adaboost_lr(i));
    ensemble_adaboost_crossval_err(i) = boosted_tree.kfoldLoss;
    ensemble_adaboost_time(i) = toc;
    save('ensemble_adaboost_crossval_err','ensemble_adaboost_crossval_err')
    save('ensemble_adaboost_time','ensemble_adaboost_time')
end

ensemble_bag_cycles =  50:10:150;
ensemble_bag_crossval_err = zeros(numel(ensemble_bag_cycles),1);
ensemble_bag_time = zeros(numel(ensemble_bag_cycles),1);

for i = 1:numel(ensemble_bag_cycles)
    tic
    bagged_tree = fitcensemble(data_train,'K','Method','Bag','CrossVal','on','NumLearningCycles',ensemble_bag_cycles(i));
    ensemble_bag_crossval_err(i) = bagged_tree.kfoldLoss;
    ensemble_bag_time(i) = toc;
    save('ensemble_bag_crossval_err','ensemble_bag_crossval_err')
    save('ensemble_bag_time','ensemble_bag_time')
end

figure
subplot(2,1,1)
plot(ensemble_adaboost_lr,ensemble_adaboost_crossval_err,'-kd')
xlabel('Learning rate')
ylabel('Crossvalidation error')
title('ADA Boost')
grid on
box on
subplot(2,1,2)
plot(ensemble_bag_cycles,ensemble_bag_crossval_err,'-kd')
xlabel('Num Cycles')
ylabel('Crossvalidation error')
title('Bagging')
grid on
box on
set(gcf,'Color','w')
saveas(gcf,'boost_err.png')

figure
subplot(2,1,1)
plot(ensemble_adaboost_lr,ensemble_adaboost_time,'-kd')
xlabel('Learning rate')
ylabel('Training time (10 folds)')
title('ADA Boost')
grid on
box on
subplot(2,1,2)
plot(ensemble_bag_cycles,ensemble_bag_time,'-kd')
xlabel('Num Cycles')
ylabel('Training time (10 folds)')
title('Bagging')
grid on
box on
set(gcf,'Color','w')
saveas(gcf,'boost_time.png')

%% Prune the tree
if min(ensemble_bag_crossval_err) < min(ensemble_adaboost_crossval_err)
    best_boosting = 'bagging';
    [~,min_bag_idx] = min(ensemble_bag_crossval_err);
    best_cycles = ensemble_bag_cycles(min_bag_idx);
else
    best_boosting = 'adaboost';
    [~,min_adaboost_idx] = min(ensemble_adaboost_crossval_err);
    best_lr = ensemble_adaboost_lr(min_adaboost_idx);
end

max_splits = [10:10:90 100:100:1000];
opt_splits_cv = zeros(numel(max_splits),1);
for i = 1:numel(max_splits)
    t = templateTree('Prune','off','MaxNumSplits',max_splits(i));
    if strcmp(best_boosting,'bagging')
        ensemble_pred = fitcensemble(data_train,'K','Method','Bag','NumLearningCycles',best_cycles,'Learners',t,'CrossVal','on');
    elseif strcmp(best_boosting,'adaboost')
        ensemble_pred = fitcensemble(data_train,'K','Method','AdaBoostM1','LearnRate',best_lr,'Learners',t,'CrossVal','on');
    end
        opt_splits_cv(i) = ensemble_pred.kfoldLoss;
end

figure
plot(max_splits,opt_splits_cv,'-kd')
xlabel('Max Splits in Tree')
ylabel('Crossvalidation error')
set(gcf,'Color','w')
grid on
box on
saveas(gcf,'pruning.png')

%% Boosting Learning Curve
lc_boost_cv_error = zeros(numel(train_split),1);
lc_boost_test_error = zeros(numel(train_split),1);
lc_test_perf = cell(numel(train_split),1);
t_best = templateTree('Prune','off','MaxNumSplits',300);

for ii = 1:numel(train_split)
    [lc_train_idx,lc_test_idx] = dividerand(size(data,1),train_split(ii),test_split(ii));
    lc_train = data(lc_train_idx,:);
    lc_test = data(lc_test_idx,:);
    if strcmp(best_boosting,'bagging')
        boosted_cv = fitcensemble(lc_train,'K','Method','Bag','Learners',t_best,'CrossVal','on','NumLearningCycles',best_cycles);
        boosted_fit = fitcensemble(lc_train,'K','Method','Bag','NumLearningCycles',best_cycles,'Learners',t_best);
    elseif strcmp(best_boosting,'adaboost')
        boosted_cv = fitcensemble(lc_train,'K','Method','AdaBoostM1','Learners',t_best,'CrossVal','on','LearnRate',best_lr);
        boosted_fit = fitcensemble(lc_train,'K','Method','AdaBoostM1','Learners',t_best,'CrossVal','off','LearnRate',best_lr);
    end
    lc_boost_cv_error(ii) = boosted_cv.kfoldLoss;
    boosted_pred = boosted_fit.predict(lc_test);
    boosted_perf = classperf(lc_test.K,boosted_pred);
    lc_boost_test_error(ii) = boosted_perf.ErrorRate;
end

save('lc_boost_cv_error','lc_boost_cv_error')
save('lc_boost_test_error','lc_boost_test_error')

figure
hold on
plot(train_split,lc_boost_cv_error,'-kd')
plot(train_split,lc_boost_test_error,'-bd')
grid on
box on
legend('Crossvalidation error','Test error')
xlabel('Training split')
set(gcf,'Color','w')
saveas(gcf,'boost_lc.png');

%% Confusion matrices
starting_tree = fitcensemble(data_train,'K','Method','Bag');
pruned_tree = fitcensemble(data_train,'K','Method','Bag','NumLearningCycles',best_cycles,'Learners',t_best);

plotconfusion(data_train.K',starting_tree.predict(data_train)')
saveas(gcf,'ensemble_start_confusion.png')
plotconfusion(data_test.K',starting_tree.predict(data_test)')
saveas(gcf,'ensemble_start_confusion_t.png')

plotconfusion(data_train.K',pruned_tree.predict(data_train)')
saveas(gcf,'ensemble_best_confusion.png')
plotconfusion(data_test.K',pruned_tree.predict(data_test)')
saveas(gcf,'ensemble_best_confusion_t.png')

cd(cdir)