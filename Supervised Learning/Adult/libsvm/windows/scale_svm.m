function status = scale_svm(fname)
    str_cmd = {'svm-scale '};
    str_cmd = strcat(str_cmd,fname,{' > '},fname,{'.scale'});
    status = system(str_cmd{1});
end