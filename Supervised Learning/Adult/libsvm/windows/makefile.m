function makefile(pred,class,fname)

class_str = cell(size(class,1),size(class,2));
class_str(class == 0) = {'-1 '};
class_str(class == 1) = {'+1 '};

pred_str = arrayfun(@num2str,pred,'UniformOutput',0);
for c = 1:size(pred,2)
    pred_str(strcmp(pred_str(:,c),'NaN'),c) = {'0'};
    pred_str(:,c) = strcat(num2str(c),':',pred_str(:,c));
end
pred_str = join(pred_str);

file_str = join(strcat(class_str,pred_str),'\n');

fid = fopen(fname,'wt');
fprintf(fid, char(file_str));
fclose(fid);
end