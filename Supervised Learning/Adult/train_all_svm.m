clc
clear
close force all
load_train
load_test
data = vertcat(adult_train,adult_test);
    
rng(0112358)
[train_idx,test_idx] = dividerand(size(data,1),0.70,0.30);
data_train = data(train_idx,:);
data_test = data(test_idx,:);

train_split = 0.90:-0.10:0.10;
test_split  = 0.10:0.10:0.90;

%% SVM
data_train_svm = data_train;
data_test_svm = data_test;
cat_vars = {'workclass','education','marital_status','occupation',...
    'relationship','race','sex','native_country'};
for v = 1:numel(cat_vars)
    eval(['data_test_svm.' cat_vars{v} '=grp2idx(data_test_svm.' cat_vars{v} ');'])
    eval(['data_train_svm.' cat_vars{v} '=grp2idx(data_train_svm.' cat_vars{v} ');'])
end
inputs_train_svm = table2array(data_train_svm(:,1:end-1));
output_train_svm = table2array(data_train_svm(:,end));
inputs_test_svm = table2array(data_test_svm(:,1:end-1));
output_test_svm = table2array(data_test_svm(:,end));
cdir = pwd;

run_svm_opt1 = 1;
run_svm_opt2 = 1;
run_svm_opt3 = 1;

% Switch to libsvm environment
cd libsvm\windows
% Create input files
makefile(inputs_train_svm,output_train_svm,'data_train')
makefile(inputs_test_svm,output_test_svm,'data_test')
% Scale inputs
scale_svm('data_train');
scale_svm('data_test');

% Create configuration for training of linear SVM
if run_svm_opt1
    svm_options.crossval = 5;
    svm_nu = 0.5:0.1:1.0;
    svm_linear_crossval_err = zeros(numel(svm_nu),1);
    svm_opt1_time = zeros(numel(svm_nu),1);
    for i = 1:numel(svm_nu)
        svm_options.nu = svm_nu(i);
        tic
        [out_status, out_cmdout] = train_svm('one-class','linear','data_train.scale',svm_options);
        svm_opt1_time(i) = toc;
        svm_cv_regexp = regexp(out_cmdout,'Cross Validation Accuracy = ([0-9]{1,}\.[0-9]{1,})\%','tokens');
        svm_linear_crossval_err(i) = 1-str2double(svm_cv_regexp{1})/100;
    end
    svm_opt1_out = [svm_nu' svm_linear_crossval_err];
    svm_opt1_out = array2table(svm_opt1_out,'VariableNames',{'nu','cv_err'});
    save('svm_linear_crossval_err','svm_linear_crossval_err')
    save('svm_opt1_time','svm_opt1_time')
    
    figure
    plot(svm_nu,svm_linear_crossval_err,'-kd')
    grid on
    box on
    xlabel('\nu')
    ylabel('Crossvalidation error')
    set(gcf,'Color','w')
    saveas(gcf,'svm_opt1_cv.png')
    
    figure
    plot(svm_nu,svm_opt1_time,'-kd')
    grid on
    box on
    xlabel('\nu')
    ylabel('Training time (5 folds)')
    set(gcf,'Color','w')
    saveas(gcf,'svm_opt1_time.png')
end

if run_svm_opt2
    clear svm_options
    svm_options.crossval = 5;
    svm_cost = exp(-10:5:10);
    svm_gamma = exp(-10:5:10);
    svm_radial_crossval_err = zeros(numel(svm_cost),numel(svm_gamma));
    svm_opt2_time = zeros(numel(svm_cost),numel(svm_gamma));
    for i = 1:numel(svm_cost)
        for j = 1:numel(svm_gamma)
            svm_options.cost = svm_cost(i);
            svm_options.gamma = svm_gamma(j);
            tic
            [out_status, out_cmdout] = train_svm('C-SVC','radial','data_train.scale',svm_options);
            svm_opt2_time(i,j) = toc;
            svm_cv_regexp = regexp(out_cmdout,'Cross Validation Accuracy = ([0-9]{1,}\.[0-9]{1,})\%','tokens');
            svm_radial_crossval_err(i,j) = 1-str2double(svm_cv_regexp{1})/100;
            disp(svm_radial_crossval_err(i,j));
        end
    end
    [svm_opt2_X,svm_opt2_Y] = meshgrid(svm_cost,svm_gamma);
    svm_opt2_X = reshape(svm_opt2_X,25,1);
    svm_opt2_Y = reshape(svm_opt2_Y,25,1);
    svm_opt2_Z = reshape(svm_radial_crossval_err',25,1);
    svm_opt2_tri = delaunay(svm_opt2_X,svm_opt2_Y);
    
    % Get optimal gamma and cost
    svm_opt2_out = [svm_opt2_Z svm_opt2_X svm_opt2_Y];
    svm_opt2_out = array2table(svm_opt2_out,'VariableNames',{'cv_err','cost','gamma'});
    svm_opt2_out = sortrows(svm_opt2_out);
    svm_opt2_best_cv = svm_opt2_out.cv_err(1); % 0.149092
    svm_opt2_best_cost = svm_opt2_out.cost(1); % 22026.4657948067
    svm_opt2_best_gamma = svm_opt2_out.gamma(1); % 0.00673794699908547
    
    save('svm_radial_crossval_err','svm_radial_crossval_err')
    save('svm_opt2_time','svm_opt2_time')
    
    figure
    hold on
    grid on
    box on
    trimesh(svm_opt2_tri,svm_opt2_X,svm_opt2_Y,svm_opt2_Z)
    scatter3(svm_opt2_X,svm_opt2_Y,svm_opt2_Z,'r','filled')
    set(gca,'yscale','log')
    set(gca,'xscale','log')
    set(gca,'zscale','log')
    view(45,45)
    xlabel('\it c')
    ylabel('\gamma')
    zlabel('Crossvalidation error')
    set(gcf,'Color','w')
    saveas(gcf,'svm_opt2_cv.png')
    
    figure
    hold on
    grid on
    box on
    svm_opt2_Ztime = reshape(svm_opt2_time',25,1);
    trimesh(svm_opt2_tri,svm_opt2_X,svm_opt2_Y,svm_opt2_Ztime)
    scatter3(svm_opt2_X,svm_opt2_Y,svm_opt2_Ztime,'r','filled')
    set(gca,'yscale','log')
    set(gca,'xscale','log')
    set(gca,'zscale','log')
    view(-45,45)
    xlabel('\it c')
    ylabel('\gamma')
    zlabel('Training time (5 folds)')
    set(gcf,'Color','w')
    saveas(gcf,'svm_opt2_time.png')
end

if run_svm_opt3
    clear svm_options
    svm_options.crossval = 5;
    svm_cost2 = exp(5:11);
    svm_gamma2 = exp(-10:-1);
    svm_radial_crossval_err2 = zeros(numel(svm_cost2),numel(svm_gamma2));
    svm_opt3_time = zeros(numel(svm_cost2),numel(svm_gamma2));
    for i = 1:numel(svm_cost2)
        for j = 1:numel(svm_gamma2)
            svm_options.cost = svm_cost2(i);
            svm_options.gamma = svm_gamma2(j);
            tic
            try
                [out_status, out_cmdout] = train_svm('C-SVC','radial','data_train.scale',svm_options);
                svm_cv_regexp = regexp(out_cmdout,'Cross Validation Accuracy = ([0-9]{1,}\.[0-9]{1,})\%','tokens');
                svm_radial_crossval_err2(i,j) = 1-str2double(svm_cv_regexp{1})/100;
                disp(svm_radial_crossval_err2(i,j));
            catch
                svm_radial_crossval_err2(i,j) = NaN;
            end
            save('svm_radial_crossval_err2','svm_radial_crossval_err2')
            save('svm_opt3_time','svm_opt3_time')
            svm_opt3_time(i,j) = toc;
        end
    end
    [svm_opt3_X,svm_opt3_Y] = meshgrid(svm_cost2,svm_gamma2);
    svm_opt3_X = reshape(svm_opt3_X,numel(svm_cost2)*numel(svm_gamma2),1);
    svm_opt3_Y = reshape(svm_opt3_Y,numel(svm_cost2)*numel(svm_gamma2),1);
    svm_opt3_Z = reshape(svm_radial_crossval_err2',numel(svm_cost2)*numel(svm_gamma2),1);
    svm_opt3_tri = delaunay(svm_opt3_X,svm_opt3_Y);
    
    % Get optimal gamma and cost
    svm_opt3_out = [svm_opt3_Z svm_opt3_X svm_opt3_Y];
    svm_opt3_out = array2table(svm_opt3_out,'VariableNames',{'cv_err','cost','gamma'});
    svm_opt3_out = sortrows(svm_opt3_out);
    svm_opt3_best_cv = svm_opt3_out.cv_err(1);
    svm_opt3_best_cost = svm_opt3_out.cost(1);
    svm_opt3_best_gamma = svm_opt3_out.gamma(1);
    
    save('svm_radial_crossval_err2','svm_radial_crossval_err2')
    save('svm_opt3_time','svm_opt3_time')
    
    figure
    hold on
    grid on
    box on
    trimesh(svm_opt3_tri,svm_opt3_X,svm_opt3_Y,svm_opt3_Z)
    scatter3(svm_opt3_X,svm_opt3_Y,svm_opt3_Z,'r','filled')
    set(gca,'yscale','log')
    set(gca,'xscale','log')
    set(gca,'zscale','log')
    view(45,45)
    xlabel('\it c')
    ylabel('\gamma')
    zlabel('Crossvalidation error')
    set(gcf,'Color','w')
    saveas(gcf,'svm_opt3_cv.png')
    
    figure
    hold on
    grid on
    box on
    svm_opt3_Ztime = reshape(svm_opt3_time',numel(svm_cost2)*numel(svm_gamma2),1);
    trimesh(svm_opt3_tri,svm_opt3_X,svm_opt3_Y,svm_opt3_Ztime)
    scatter3(svm_opt3_X,svm_opt3_Y,svm_opt3_Ztime','r','filled')
    set(gca,'yscale','log')
    set(gca,'xscale','log')
    set(gca,'zscale','log')
    view(-45,45)
    xlabel('\it c')
    ylabel('\gamma')
    zlabel('Training time (5 folds)')
    set(gcf,'Color','w')
    saveas(gcf,'svm_opt3_time.png')
end

%% SVM Learning Curve
svm_options.cost = svm_opt3_best_cost;
svm_options.gamma = svm_opt3_best_gamma;

lc_svm_cv_error = zeros(numel(train_split),1);
lc_svm_test_error = zeros(numel(train_split),1);
lc_test_perf = cell(numel(train_split),1);

for ii = 2:numel(train_split)
    [lc_train_idx,lc_test_idx] = dividerand(size(data,1),train_split(ii),test_split(ii));
    lc_train = data(lc_train_idx,:);
    lc_test = data(lc_test_idx,:);
    for v = 1:numel(cat_vars)
        eval(['lc_test.' cat_vars{v} '=grp2idx(lc_test.' cat_vars{v} ');'])
        eval(['lc_train.' cat_vars{v} '=grp2idx(lc_train.' cat_vars{v} ');'])
    end
    inputs_train_lc = table2array(lc_train(:,1:end-1));
    output_train_lc = table2array(lc_train(:,end));
    inputs_test_lc = table2array(lc_test(:,1:end-1));
    output_test_lc = table2array(lc_test(:,end));
    % Create input files
    makefile(inputs_train_lc,output_train_lc,'lc_train')
    makefile(inputs_test_lc,output_test_lc,'lc_test')
    % Scale inputs
    scale_svm('lc_train');
    scale_svm('lc_test');
    svm_options.crossval = 5;
    [~, out_cmdout] = train_svm('C-SVC','radial','lc_train.scale',svm_options);
    svm_cv_regexp = regexp(out_cmdout,'Cross Validation Accuracy = ([0-9]{1,}\.[0-9]{1,})\%','tokens');
    lc_svm_cv_error(ii) = 1-str2double(svm_cv_regexp{1})/100;
    svm_options.crossval = 0;
    train_svm('C-SVC','radial','lc_train.scale',svm_options);
    [~, out_cmdout] = pred_svm('lc_test.scale','lc_train.scale.model','lc_test.pred');
    svm_cv_regexp = regexp(out_cmdout,'Accuracy = ([0-9]{1,}\.[0-9]{1,})\%','tokens');
    lc_svm_test_error(ii) = 1-str2double(svm_cv_regexp{1})/100;
end

save('lc_svm_cv_error','lc_svm_cv_error')
save('lc_svm_test_error','lc_svm_test_error')

figure
hold on
plot(train_split,lc_svm_cv_error,'-kd')
plot(train_split,lc_svm_test_error,'-bd')
grid on
box on
legend('Crossvalidation error','Test error')
xlabel('Training split')
set(gcf,'Color','w')
saveas(gcf,'svm_lc.png');

%% Confusion matrices

train_svm('C-SVC','radial','data_train.scale');
pred_svm('data_train.scale','data_train.scale.model','data_train.pred');
train_pred = read_pred('data_train.pred');
train_pred(train_pred == -1) = 0;
pred_svm('data_test.scale','data_train.scale.model','data_test.pred');
test_pred = read_pred('data_test.pred');
test_pred(test_pred == -1) = 0;

plotconfusion(output_train_svm',train_pred')
saveas(gcf,'svm_start_confusion.png')
plotconfusion(output_test_svm',test_pred')
saveas(gcf,'svm_start_confusion_t.png')

svm_options.cost = svm_opt3_best_cost;
svm_options.gamma = svm_opt3_best_gamma;
svm_options.crossval = 0;
train_svm('C-SVC','radial','data_train.scale',svm_options);
pred_svm('data_train.scale','data_train.scale.model','data_train.pred');
train_pred = read_pred('data_train.pred');
train_pred(train_pred == -1) = 0;
pred_svm('data_test.scale','data_train.scale.model','data_test.pred');
test_pred = read_pred('data_test.pred');
test_pred(test_pred == -1) = 0;

plotconfusion(output_train_svm',train_pred')
saveas(gcf,'svm_best_confusion.png')
plotconfusion(output_test_svm',test_pred')
saveas(gcf,'svm_best_confusion_t.png')

cd(cdir)