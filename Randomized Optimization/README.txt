Everything was done in Windows
ABAGAIL's JAR is compiled with Java 1.8.0_144
The directory was compressed using 7zip

----------------
NEURAL NETWORKS
----------------
The Neural Network training is done using Matlab r2017a, provided by GT.
There are 4 folders in the Neural Network directory:
- ga: genetic algorithm training
- hill: randomized hill climbing traininig
- sa: simulated annealing training
- nnet: backpropagation with gradient descent training
Each folder contains the outputs for the distinct runs in txt file, as well as the plots and workspaces in the end of the run.

----------------------
OPTIMIZATION PROBLEMS
----------------------
The directory 'Optimization Problems' contains the Jython code for each problem as well as the directory for ABAGAIL
The 'output' directory contains the results for each algorithm as txt files and a directory called 'OUT_DATA' that has a csv file 
with the content of all indiviudal runs, and the Matlab code used to merge the files.

To run the Jython files:
1) Install Java 1.8.0_144
2) Install Jython 2.7.0 and Python 2.7.14
3) Install Apache ant
4) Install Eclipse
5) Follow the instructions in http://www.jython.org/jythonbook/en/1.0/JythonIDE.html#eclipse to configure Eclipse with Jython
6) Compile ABAGAIL with ant or use the provided JAR
7) Create a directory called 'output' in C:\ with three subdirectories with names 'countones', 'fourpeaks' and 'knapsack'
7) Run the file as usual in Eclipse

-------
REPORT
-------
The plots in the report for the neural networks were created in Matlab, the rest of the plots were created in R.
The 'Report Code' directory has the file with the code for creating the plots with ggplot and report.
The generation of the report requires MyKTeX and several additional R libraries and LaTeX packages.