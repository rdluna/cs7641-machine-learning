%% Optimize with selected parameters
ga_opts.CrossoverFraction = values_mat(best_idx,1);
ga_opts.EliteCount = values_mat(best_idx,2);
ga_opts.InitialPopulation = x(:,best_idx)';
ga_opts.TimeLimit = 1800;
ga_opts.StallTimeLimit = 300;
diary('output_ga_best.txt')
[x_ga_opt, err_ga] = ga(fun2, 122, ga_opts);
diary off
best_data = load_output('output_ga_best.txt');
save 'workspace_ga'