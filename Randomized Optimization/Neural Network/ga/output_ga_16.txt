{Error using <a href="matlab:matlab.internal.language.introspective.errorDocCallback('validate', 'C:\Program Files\MATLAB\R2017a\toolbox\globaloptim\globaloptim\private\validate.m', 82)" style="font-weight:bold">validate</a> (<a href="matlab: opentoline('C:\Program Files\MATLAB\R2017a\toolbox\globaloptim\globaloptim\private\validate.m',82,0)">line 82</a>)
Elite count must be less than Population Size.

Error in <a href="matlab:matlab.internal.language.introspective.errorDocCallback('gacommon', 'C:\Program Files\MATLAB\R2017a\toolbox\globaloptim\globaloptim\private\gacommon.m', 65)" style="font-weight:bold">gacommon</a> (<a href="matlab: opentoline('C:\Program Files\MATLAB\R2017a\toolbox\globaloptim\globaloptim\private\gacommon.m',65,0)">line 65</a>)
[options,nvars,FitnessFcn,NonconFcn] =
validate(options,type,nvars,fun,nonlcon,user_options);

Error in <a href="matlab:matlab.internal.language.introspective.errorDocCallback('ga', 'C:\Program Files\MATLAB\R2017a\toolbox\globaloptim\globaloptim\ga.m', 336)" style="font-weight:bold">ga</a> (<a href="matlab: opentoline('C:\Program Files\MATLAB\R2017a\toolbox\globaloptim\globaloptim\ga.m',336,0)">line 336</a>)
    NonconFcn,options,Iterate,type] =
    gacommon(nvars,fun,Aineq,bineq,Aeq,beq,lb,ub, ...

Error in <a href="matlab:matlab.internal.language.introspective.errorDocCallback('ganet', 'C:\Users\rodri\Documents\MATLAB\TicTacToe\Assignment 2\ga\ganet.m', 70)" style="font-weight:bold">ganet</a> (<a href="matlab: opentoline('C:\Users\rodri\Documents\MATLAB\TicTacToe\Assignment 2\ga\ganet.m',70,0)">line 70</a>)
        x(:,i) = ga(fun1, 122, ga_opts);
} 
j

j =

     6

ganet

                                  Best           Mean      Stall
Generation      Func-count        f(x)           f(x)    Generations
    1              400          0.4258           4.687        0
    2              600          0.4258           4.706        1
    3              800          0.4258           4.683        2
    4             1000          0.4258           4.572        3
    5             1200          0.4258           4.679        4
    6             1400          0.4258             4.6        5
    7             1600          0.4258           4.549        6
    8             1800          0.4258           4.532        7
    9             2000          0.4258           4.524        8
   10             2200          0.4258           4.545        9
   11             2400          0.4258           4.543       10
   12             2600          0.4258           4.555       11
   13             2800          0.4258           4.463       12
   14             3000          0.4258           4.505       13
   15             3200          0.4258            4.48       14
   16             3400          0.4258           4.455       15
   17             3600          0.4258           4.431       16
   18             3800          0.4258           4.396       17
   19             4000          0.4258           4.342       18
   20             4200          0.4258           4.453       19
   21             4400          0.4258           4.381       20
   22             4600          0.4258           4.353       21
   23             4800          0.4258           4.326       22
   24             5000          0.4258           4.326       23
   25             5200          0.4258            4.33       24
   26             5400          0.4258           4.277       25
   27             5600          0.4258            4.26       26
   28             5800          0.4258           4.273       27
   29             6000          0.4258           4.189       28
   30             6200          0.4258           4.238       29

                                  Best           Mean      Stall
Generation      Func-count        f(x)           f(x)    Generations
   31             6400          0.4258           4.231       30
   32             6600          0.4258           4.217       31
   33             6800          0.4258           4.186       32
   34             7000          0.4258           4.161       33
   35             7200          0.4258           4.125       34
   36             7400          0.4258           4.146       35
   37             7600          0.4258           4.142       36
   38             7800          0.4258           4.082       37
Optimization terminated: stall time limit exceeded.

                                  Best           Mean      Stall
Generation      Func-count        f(x)           f(x)    Generations
    1              400          0.4258           4.808        0
    2              600          0.4258           4.768        1
    3              800          0.4258           4.766        2
    4             1000          0.4258           4.773        3
    5             1200          0.4258           4.804        4
    6             1400          0.4258           4.751        5
    7             1600          0.4258           4.649        6
    8             1800          0.4258           4.649        7
    9             2000          0.4258           4.685        8
   10             2200          0.4258           4.615        9
   11             2400          0.4258           4.547       10
   12             2600          0.4258           4.568       11
   13             2800          0.4258           4.497       12
   14             3000          0.4258           4.534       13
   15             3200          0.4258           4.515       14
   16             3400          0.4258           4.471       15
   17             3600          0.4258           4.469       16
   18             3800          0.4258           4.491       17
   19             4000          0.4258           4.458       18
   20             4200          0.4258           4.458       19
   21             4400          0.4258           4.426       20
   22             4600          0.4258           4.432       21
   23             4800          0.4258           4.395       22
   24             5000          0.4258           4.329       23
   25             5200          0.4258           4.324       24
   26             5400          0.4258           4.357       25
   27             5600          0.4258           4.253       26
   28             5800          0.4258           4.301       27
   29             6000          0.4258           4.263       28
   30             6200          0.4258           4.297       29

                                  Best           Mean      Stall
Generation      Func-count        f(x)           f(x)    Generations
   31             6400          0.4258           4.205       30
   32             6600          0.4258           4.242       31
   33             6800          0.4258           4.169       32
   34             7000          0.4258             4.2       33
   35             7200          0.4258           4.121       34
   36             7400          0.4258           4.119       35
   37             7600          0.4258           4.154       36
   38             7800          0.4258           4.148       37
   39             8000          0.4258           4.138       38
Optimization terminated: stall time limit exceeded.
