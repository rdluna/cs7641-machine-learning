XData = best_data.generation;
YData = best_data.funcount;
crossentropy_vals = table2array(readtable('crossentropy_vals.txt'));
CrData = crossentropy_vals;

idx = YData;
last_pt = 1;

npts = zeros(numel(XData),1);
best_ce = zeros(numel(XData),2);

for i = 1:numel(XData)
    disp([last_pt idx(i)])
    current_data = CrData(last_pt:idx(i),:);
    npts(i) = (numel(current_data)/2);
    last_pt = idx(i)+1;
    current_data = sortrows(current_data);
    best_ce(i,:) = current_data(1,:);
end

hFig = figure;
hold on
plot(XData,best_ce(:,1),'b')
plot(XData,best_ce(:,2),'g')
legend('Training set','Test set')
ylabel('Cross entropy')
xlabel('Iteration')
xlim([0 250])
ylim([0.22 0.36])
grid on
box on
set(hFig,'Color','w')
saveas(hFig,'best_ce.png')
close(hFig)

clear sp1data