function state = gaplotcustom(options,state,flag)
%GAPLOTBESTF Plots the best score and the mean score.
%   STATE = GAPLOTBESTF(OPTIONS,STATE,FLAG) plots the best score as well
%   as the mean of the scores.
%
%   Example:
%    Create an options structure that will use GAPLOTBESTF
%    as the plot function
%     options = optimoptions('ga','PlotFcn',@gaplotbestf);

%   Copyright 2003-2016 The MathWorks, Inc.

if size(state.Score,2) > 1
    msg = getString(message('globaloptim:gaplotcommon:PlotFcnUnavailable','gaplotbestf'));
    title(msg,'interp','none');
    return;
end

switch flag
    case 'init'
        hold on;
        set(gca,'xlim',[0,options.MaxGenerations]);
        xlabel('Generation','interp','none');
        ylabel('Fitness value','interp','none');
        crossEntropy = sortrows(readtable('crossentropy_vals.txt'));
        plotTrain = plot(state.Generation,crossEntropy{1,1},'.r');
        set(plotTrain,'Tag','gaplottrain');
        plotTest = plot(state.Generation,crossEntropy{1,2},'.g');
        set(plotTest,'Tag','gaplottest');
        fopen('crossentropy_vals.txt','w');
        fclose('all');
    case 'iter'
        plotTrain = findobj(get(gca,'Children'),'Tag','gaplottrain');
        plotTest = findobj(get(gca,'Children'),'Tag','gaplottest');
        last_train = get(plotTrain,'Ydata');
        last_train = last_train(end);
        last_test = get(plotTest,'Ydata');
        last_test = last_test(end);
        newX = [get(plotTrain,'Xdata') state.Generation];
        crossEntropy = sortrows(readtable('crossentropy_vals.txt'));
        if crossEntropy{1,1} < last_train
            bestTrain = crossEntropy{1,1};
            bestTest = crossEntropy{1,2};
        else
            bestTrain = last_train;
            bestTest = last_test;
        end
        newY = [get(plotTrain,'Ydata') bestTrain];
        set(plotTrain,'Xdata',newX, 'Ydata',newY);
        newY = [get(plotTest,'Ydata') bestTest];
        set(plotTest,'Xdata',newX, 'Ydata',newY);
        fopen('crossentropy_vals.txt','w');
        fclose('all');
    case 'done'
        LegnD = legend('Training set','Test set');
        set(LegnD,'FontSize',8);
        hold off;
end
