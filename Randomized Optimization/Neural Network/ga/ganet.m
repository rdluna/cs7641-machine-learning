clc
clear all
close force all
load_tictactoe

v = data.Properties.VariableNames(1:end-1);
for i = 1:numel(v)
    eval(['data.' v{i} '=grp2idx(data.' v{i} ');']);
end

rng(0112358)
[train_idx,test_idx] = dividerand(size(data,1),0.70,0.30);
data_train = data(train_idx,:);
data_test = data(test_idx,:);

train_split = 0.90:-0.10:0.10;
test_split  = 0.10:0.10:0.90;

data_train_nn = data_train;
data_test_nn = data_test;
inputs_train_nn = table2array(data_train_nn(:,1:end-1));
output_train_nn = table2array(data_train_nn(:,end));
inputs_test_nn = table2array(data_test_nn(:,1:end-1));
output_test_nn = table2array(data_test_nn(:,end));
output_train_nn(:,2) = ~output_train_nn;
output_test_nn(:,2) = ~output_test_nn;

%% Genetic Algorithm
% Number of neurons
n = 10;

Inputs = inputs_train_nn';
Targets = output_train_nn';
Inputs_T = inputs_test_nn';
Targets_T = output_test_nn';

% Number of attributes and number of classifications
[n_attr, ~]  = size(Inputs);
[n_class, ~] = size(Targets);

% Initialize neural network
net = patternnet(n,'traingd','crossentropy');

% Configure the neural network for this dataset
net = configure(net, Inputs, Targets); %view(net);

fun1 = @(x) crossentropy_eval1(x, net, Inputs, Targets, Inputs_T, Targets_T);
fun2 = @(x) crossentropy_eval2(x, net, Inputs, Targets, Inputs_T, Targets_T);

% Setting the Genetic Algorithms tolerance for
% minimum change in fitness function before
% terminating algorithm to 1e-8 and displaying
% each iteration's results.
rng(1)
InitialPopulationMatrix = rand(122,1)';
ga_opts = gaoptimset('InitialPopulation',InitialPopulationMatrix,'TimeLimit',300,'TolFun',1e-8,'Display','iter','PlotFcn',{@gaplotbestf,@gaplotselection},'UseParallel',true,'StallTimeLimit',120);

%% Define best parameters
crossoverFraction = [0.10 0.25 0.5 0.75];
eliteCount = floor([0.0125 0.05 0.10 0.20 0.25 0.30].*size(Inputs,2));
names = cell(numel(crossoverFraction)*numel(eliteCount),1);
x = zeros(122,numel(crossoverFraction)*numel(eliteCount));

for i = 1:numel(crossoverFraction)
    for j = 1:numel(eliteCount)
        id = strcat(num2str(i),num2str(j));
        ga_opts.CrossoverFraction = crossoverFraction(i);
        ga_opts.EliteCount = eliteCount(j);
        diary(strcat('output_ga_',id,'.txt'))
        x(:,i) = ga(fun1, 122, ga_opts);
        diary off
        names{i*j} = strcat(num2str(crossoverFraction(i)),'_',num2str(eliteCount(j)));
    end
end

%%
co = [247,247,247;...
      204,204,204;...
      150,150,150;...
      99,99,99;
      37,37,37]./255;


files = dir('output_ga_*');
files = cellstr(char(files(:).name));
files(strcmp('output_ga_best.txt',files)) = [];
names = cellfun(@(x) x(end-5:end-4),files,'UniformOutput',0);
best_val = zeros(numel(crossoverFraction)*numel(eliteCount),1);
figure
for i = 1:numel(files)
    file = files{i};
    data = load_output(file);
    best_val(i) = data.bestf(end);
end

[best, best_idx] = min(best_val);
values_mat = [reshape(repmat(crossoverFraction,6,1),24,1),...
    repmat(eliteCount',4,1)];

%% Optimize with selected parameters
ga_opts.PlotFcns = {@gaplotcustom};
ga_opts.CrossoverFraction = values_mat(best_idx,1);
ga_opts.EliteCount = values_mat(best_idx,2);
ga_opts.TimeLimit = 1800;
ga_opts.StallTimeLimit = 300;
ga_opts.TolFun = 1e-16;
diary('output_ga_best.txt')
[x_ga_opt, err_ga] = ga(fun2, 122, ga_opts);
diary off
save 'workspace_ga'

saveas(gcf,'ga_plots.fig')
spdata = findobj(gcf, 'Type', 'line');

figure
hold on
plot(spdata(2).XData,spdata(2).YData,'g')
plot(spdata(1).XData,spdata(1).YData,'b')
grid on
box on
set(gcf,'Color','w')
xlabel('Generation')
ylabel('Cross entropy')
legend('Training set','Test set')
title(['Best training: ' num2str(spdata(2).YData(end)) ', Test at best training: ' num2str(spdata(1).YData(end))])
saveas(gcf,'bestf.png')


figure
bar(x_ga_opt)
xlim([0 122])
grid on
set(gcf,'Color','w')
xlabel('Element')
ylabel('Best value')
saveas(gcf,'bestx.png')
