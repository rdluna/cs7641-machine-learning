sp1data = findobj(subplot(2,2,1), 'Type', 'line');
sp2data = findobj(subplot(2,2,2), 'Type', 'line');
sp3data = findobj(subplot(2,2,4), 'Type', 'line');

close force all

figure
XData = sp1data.XData;
YData = sp1data.YData;
scatter(XData,YData,'md','filled','markeredgecolor','k')
xlabel('Iteration')
ylabel('Function count')
grid on
box on
set(gcf,'Color','w')
saveas(gcf,'funcount.png')
clear sp1data

figure
XData = sp2data.XData;
YData = sp2data.YData;
plot(XData,YData,'b')
xlabel('Iteration')
ylabel('Mesh size')
grid on
box on
set(gcf,'Color','w')
saveas(gcf,'meshsize.png')
clear sp2data

figure
hold on
XData1 = sp3data(1).XData;
YData1 = sp3data(1).YData;
XData2 = sp3data(2).XData;
YData2 = sp3data(2).YData;
plot(XData2,YData2,'g')
plot(XData1,YData1,'b')
xlabel('Iteration')
ylabel('Cross entropy')
legend('Training set','Test set')
title(['Best training: ' num2str(YData2(end)) ', Test at best training: ' num2str(YData1(end))]);
grid on
box on
set(gcf,'Color','w')
saveas(gcf,'bestf.png')
clear sp3data
