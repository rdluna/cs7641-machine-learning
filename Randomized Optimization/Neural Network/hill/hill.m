clc
clear all
close force all
load_tictactoe

v = data.Properties.VariableNames(1:end-1);
for i = 1:numel(v)
    eval(['data.' v{i} '=grp2idx(data.' v{i} ');']);
end

rng(0112358)
[train_idx,test_idx] = dividerand(size(data,1),0.70,0.30);
data_train = data(train_idx,:);
data_test = data(test_idx,:);

train_split = 0.90:-0.10:0.10;
test_split  = 0.10:0.10:0.90;

data_train_nn = data_train;
data_test_nn = data_test;
inputs_train_nn = table2array(data_train_nn(:,1:end-1));
output_train_nn = table2array(data_train_nn(:,end));
inputs_test_nn = table2array(data_test_nn(:,1:end-1));
output_test_nn = table2array(data_test_nn(:,end));
output_train_nn(:,2) = ~output_train_nn;
output_test_nn(:,2) = ~output_test_nn;

%% Hill Climbing
% Number of neurons
n = 10;

Inputs = inputs_train_nn';
Targets = output_train_nn';
Inputs_T = inputs_test_nn';
Targets_T = output_test_nn';

% Number of attributes and number of classifications
[n_attr, ~]  = size(Inputs);
[n_class, ~] = size(Targets);

% Initialize neural network
net = patternnet(n,'traingd','crossentropy');

% Configure the neural network for this dataset
net = configure(net, Inputs, Targets); %view(net);

fun = @(w) crossentropy_eval(w, net, Inputs, Targets, Inputs_T, Targets_T);

% Add 'Display' option to display result of iterations
ps_opts = psoptimset ('CompletePoll', 'off', ...
                      'Display', 'iter',...
                      'PlotFcn', {@psplotfuncount,@psplotmeshsize,@psplotbestf,@psplotcustom},...
                      'TimeLimit',1800);

% There is n_attr attributes in dataset, and there are n neurons so there 
% are total of n_attr*n input weights (uniform weight)
initial_il_weights = ones(1, n_attr*n)/(n_attr*n);
% There are n bias values, one for each neuron (random)
initial_il_bias    = rand(1, n);
% There is n_class output, so there are total of n_class*n output weights 
% (uniform weight)
initial_ol_weights = ones(1, n_class*n)/(n_class*n);
% There are n_class bias values, one for each output neuron (random)
initial_ol_bias    = rand(1, n_class);
% starting values
starting_values = [initial_il_weights, initial_il_bias, ...
                   initial_ol_weights, initial_ol_bias];

diary('hill.txt')
[x, fval, flag, output] = patternsearch(fun, starting_values, [], [],[],[], -1e5, 1e5, ps_opts);
diary off
save workspace_hill
saveas(gcf,'hill_plots.fig')

figure
bar(x)
xlim([0 122])
grid on
set(gcf,'Color','w')
xlabel('Element')
ylabel('Best value')
saveas(gcf,'bestx.png')

plot_cross