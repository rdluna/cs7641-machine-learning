function crossentropy_calc = crossentropy_eval(x, net, inputs, targets, inputs_t, targets_t)

if islogical(x)
    x = double(x);
end
    
% 'x' contains the weights and biases vector
% in row vector form as passed to it by the
% genetic algorithm. This must be transposed
% when being set as the weights and biases
% vector for the network.

% To set the weights and biases vector to the
% one given as input
net = setwb(net, x');

% To evaluate the ouputs based on the given
% weights and biases vector
y = net(inputs);
y_t = net(inputs_t);

% Calculating the mean squared error (targets could have multiple
% classification (i.e. targets could be 2 dim) so take sum twice)
% and divide it by number of entries
crossentropy_calc = crossentropy(net,targets,y);
crossentropy_calc_t = crossentropy(net,targets_t,y_t);

fid = fopen('crossentropy_vals.txt', 'a');
fprintf(fid, '\n%g,%g',crossentropy_calc,crossentropy_calc_t);
fprintf(fid, '\n%g,%g',crossentropy_calc,crossentropy_calc_t);
fclose(fid);

end