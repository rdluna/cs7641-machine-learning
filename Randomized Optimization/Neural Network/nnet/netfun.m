function testval = netfun(XTRAIN, YTRAIN, XTEST, YTEST, n, l, e, lr, trainfn)

net = patternnet(10,'traingd','crossentropy');
net.trainParam.time = 600;
net.trainParam.max_fail = 10;
net.trainParam.showWindow = 0;
net.trainParam.showCommandLine = 1;
net.trainParam.lr = lr;
net.trainParam.epochs = 99e99;
net = configure(net, XTRAIN', YTRAIN');

net = train(net, XTRAIN', YTRAIN','useParallel','yes');
yNet = net(XTEST');
cp = crossentropy(net,YTEST', yNet);
testval = cp;

end