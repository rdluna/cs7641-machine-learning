function stop = saplotcustom(~,optimvalues,flag)
%SAPLOTBESTF PlotFcn to plot best function value.
%   STOP = SAPLOTBESTF(OPTIONS,OPTIMVALUES,FLAG) where OPTIMVALUES is a
%   structure with the following fields:
%              x: current point
%           fval: function value at x
%          bestx: best point found so far
%       bestfval: function value at bestx
%    temperature: current temperature
%      iteration: current iteration
%      funccount: number of function evaluations
%             t0: start time
%              k: annealing parameter
%
%   OPTIONS: The options object created by using OPTIMOPTIONS
%
%   FLAG: Current state in which PlotFcn is called. Possible values are:
%           init: initialization state
%           iter: iteration state
%           done: final state
%
%   STOP: A boolean to stop the algorithm.
%
%   Example:
%    Create an options structure that will use SAPLOTBESTF
%    as the plot function
%     options = optimoptions('simulannealbnd','PlotFcn',@saplotbestf);

%   Copyright 2006-2015 The MathWorks, Inc.
stop = false;
switch flag
    case 'init'
        hold on;
        xlabel('Iteration','interp','none');
        ylabel('Fitness value','interp','none');
        crossEntropy = sortrows(readtable('crossentropy_vals.txt'));
        plotTrain = plot(optimvalues.iteration,crossEntropy{1,1},'.r');
        set(plotTrain,'Tag','gaplottrain');
        plotTest = plot(optimvalues.iteration,crossEntropy{1,2},'.g');
        set(plotTest,'Tag','gaplottest');
        fopen('crossentropy_vals.txt','w');
        fclose('all');
    case 'iter'
        plotTrain = findobj(get(gca,'Children'),'Tag','gaplottrain');
        plotTest = findobj(get(gca,'Children'),'Tag','gaplottest');
        last_train = get(plotTrain,'Ydata');
        last_train = last_train(end);
        last_test = get(plotTest,'Ydata');
        last_test = last_test(end);
        newX = [get(plotTrain,'Xdata') optimvalues.iteration];
        crossEntropy = sortrows(readtable('crossentropy_vals.txt'));
        if crossEntropy{1,1} < last_train
            bestTrain = crossEntropy{1,1};
            bestTest = crossEntropy{1,2};
        else
            bestTrain = last_train;
            bestTest = last_test;
        end
        newY = [get(plotTrain,'Ydata') bestTrain];
        set(plotTrain,'Xdata',newX, 'Ydata',newY);
        newY = [get(plotTest,'Ydata') bestTest];
        set(plotTest,'Xdata',newX, 'Ydata',newY);
        fopen('crossentropy_vals.txt','w');
        fclose('all');
    case 'done'
        LegnD = legend('Training set','Test set');
        set(LegnD,'FontSize',8);
        hold off;
end
