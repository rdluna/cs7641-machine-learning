clc
clear all
close force all
load_tictactoe

v = data.Properties.VariableNames(1:end-1);
for i = 1:numel(v)
    eval(['data.' v{i} '=grp2idx(data.' v{i} ');']);
end

rng(0112358)
[train_idx,test_idx] = dividerand(size(data,1),0.70,0.30);
data_train = data(train_idx,:);
data_test = data(test_idx,:);

train_split = 0.90:-0.10:0.10;
test_split  = 0.10:0.10:0.90;

data_train_nn = data_train;
data_test_nn = data_test;
inputs_train_nn = table2array(data_train_nn(:,1:end-1));
output_train_nn = table2array(data_train_nn(:,end));
inputs_test_nn = table2array(data_test_nn(:,1:end-1));
output_test_nn = table2array(data_test_nn(:,end));
output_train_nn(:,2) = ~output_train_nn;
output_test_nn(:,2) = ~output_test_nn;

%% Hill Climbing
% Number of neurons
n = 10;

Inputs = inputs_train_nn';
Targets = output_train_nn';
Inputs_T = inputs_test_nn';
Targets_T = output_test_nn';

% Number of attributes and number of classifications
[n_attr, ~]  = size(Inputs);
[n_class, ~] = size(Targets);

% Initialize neural network
net = patternnet(n,'traingd','crossentropy');

% Configure the neural network for this dataset
net = configure(net, Inputs, Targets); %view(net);

fun1 = @(w) crossentropy_eval1(w, net, Inputs, Targets, Inputs_T, Targets_T);
fun2 = @(w) crossentropy_eval2(w, net, Inputs, Targets, Inputs_T, Targets_T);

% Unbounded
lb = -Inf;
ub = Inf;

% Add 'Display' option to display result of iterations
sa_opts = saoptimset('TimeLimit',300,...
    'TolFun', 1e-6,...
    'Display', 'iter',...
    'PlotFcn', {@saplottemperature,@saplotf,@saplotbestx,@saplotbestf});


% There is n_attr attributes in dataset, and there are n neurons so there
% are total of n_attr*n input weights (uniform weight)
initial_il_weights = ones(1, n_attr*n)/(n_attr*n);
% There are n bias values, one for each neuron (random)
initial_il_bias    = rand(1, n);
% There is n_class output, so there are total of n_class*n output weights
% (uniform weight)
initial_ol_weights = ones(1, n_class*n)/(n_class*n);
% There are n_class bias values, one for each output neuron (random)
initial_ol_bias    = rand(1, n_class);
% starting values
starting_values = [initial_il_weights, initial_il_bias, ...
    initial_ol_weights, initial_ol_bias];

annealingFcn = {@annealingfast,@annealingboltz};
temperatureFcn = {@temperaturefast,@temperatureboltz,@temperatureexp};
initialTemperature = [2.5 5 10 100];
reannealingInterval = [1000 10000 1e99];

%%
close force all

for i = 1:numel(annealingFcn)
    for j = 1:numel(temperatureFcn)
        for k = 1:numel(initialTemperature)
            for z = 1:numel(reannealingInterval)
                id = strcat(num2str(i),num2str(j),num2str(k),num2str(z));
                sa_opts.AnnealingFcn = annealingFcn{i};
                sa_opts.TemperatureFcn = temperatureFcn{j};
                sa_opts.InitialTemperature = initialTemperature(k);
                sa_opts.ReannealInterval = reannealingInterval(z);
                diary(strcat('output_sa_',id,'.txt'))
                simulannealbnd(fun1, starting_values, -Inf, Inf, sa_opts);
                diary off
            end
        end
    end
end

save workspace_sa

%%
files = dir('output_sa_*');
files(strcmp('output_ga_best.txt',files)) = [];
files = cellstr(char(files(:).name));
best_val = zeros(2*3*4*3,1);

for i = 1:numel(files)
    file = files{i};
    data = load_output(file);
    best_val(i) = data.bestf(end);
end

[best, best_idx] = min(best_val);
values_mat = [reshape(repmat([1 2],36,1),72,1), ...
    reshape(repmat([1 2 3],12,2),72,1), ...
    reshape(repmat(initialTemperature,3,6),72,1),...
    repmat(reannealingInterval,1,24)'];
save workspace_sa
%% Optimize with selected parameters
sa_opts.AnnealingFcn = annealingFcn{values_mat(best_idx,1)};
sa_opts.TemperatureFcn = temperatureFcn{values_mat(best_idx,2)};
sa_opts.InitialTemperature = 1.5;
sa_opts.ReannealInterval = values_mat(best_idx,4);
sa_opts.TimeLimit = 1800;
sa_opts.PlotFcns = {@saplotcustom};
close force all
diary('output_sa_best.txt')
best_x = simulannealbnd(fun2, starting_values, -Inf, Inf, sa_opts);
diary off
saveas(gcf,'sa_plots.fig')
save workspace_sa

plotdata = findobj(gcf, 'Type', 'line');

figure
hold on
XData1 = plotdata(1).XData;
YData1 = plotdata(1).YData;
XData2 = plotdata(2).XData;
YData2 = plotdata(2).YData;
plot(XData1,YData1,'g')
plot(XData2,YData2,'b')
legend('Training set','Test set')
xlabel('Iteration')
ylabel('Cross entropy')
set(gcf,'Color','w')
grid on
box on
title(['Best training: ' num2str(YData2(end)) ', Test at best training: ' num2str(YData1(end))]);
saveas(gcf,'bestf.png')

figure
best_data = load_output('output_sa_best.txt');
plot(best_data.iteration,best_data.currentf);
xlabel('Iteration')
ylabel('Function Value')
grid on
box on
set(gcf,'Color','w')
saveas(gcf,'funval.png')


figure
bar(best_x)
xlim([0 122])
grid on
set(gcf,'Color','w')
xlabel('Element')
ylabel('Best value')
saveas(gcf,'bestx.png')

