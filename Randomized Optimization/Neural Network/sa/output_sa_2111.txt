
                           Best        Current           Mean
Iteration   f-count         f(x)         f(x)         temperature
     0          1       0.349018       0.349018            2.5
    10         11       0.337781       0.339933       0.227273
    20         21       0.324783       0.327858       0.119048
    30         31       0.324235       0.324558      0.0806452
    40         41       0.321099       0.329814      0.0609756
    50         51       0.321099       0.321608      0.0490196
    60         61       0.321013       0.322389      0.0409836
    70         71       0.320535       0.321103      0.0352113
    80         81       0.320535       0.321451      0.0308642
    90         91       0.315874       0.315874      0.0274725
   100        101       0.314427       0.316393      0.0247525
   110        111       0.313304        0.31453      0.0225225
   120        121       0.312508       0.312508      0.0206612
   130        131       0.307495       0.307541       0.019084
   140        141       0.307495       0.308824      0.0177305
   150        151        0.30694        0.30694      0.0165563
   160        161        0.30694       0.310529       0.015528
   170        171        0.30694       0.312468      0.0146199
   180        181        0.30694       0.311763      0.0138122
   190        191        0.30694       0.309392       0.013089
   200        201        0.30694       0.308993      0.0124378
   210        211        0.30694       0.308534      0.0118483
   220        221        0.30694       0.307974      0.0113122
   230        231        0.30694       0.307961      0.0108225
   240        241        0.30694       0.307653      0.0103734
   250        251       0.306011       0.306011     0.00996016
   260        261       0.305814       0.307403     0.00957854
   270        271       0.305814       0.307663     0.00922509
   280        281       0.305814       0.308302      0.0088968
   290        291       0.305814       0.307365     0.00859107
   300        301       0.305814       0.307382     0.00830565

                           Best        Current           Mean
Iteration   f-count         f(x)         f(x)         temperature
   310        311       0.305814       0.307817     0.00803859
   320        321       0.305814       0.307869     0.00778816
   330        331       0.305814       0.306629     0.00755287
   340        341       0.305814       0.307209     0.00733138
   350        351       0.305814       0.306328     0.00712251
   360        361       0.305814       0.306323     0.00692521
   370        371       0.305814       0.307166     0.00673854
   380        381       0.305814       0.306581     0.00656168
   390        391       0.305814       0.306094     0.00639386
   400        401       0.305303       0.306391     0.00623441
   410        411       0.304749       0.304749     0.00608273
   420        421       0.304524       0.304524     0.00593824
   430        431       0.304183       0.304183     0.00580046
   440        441       0.304183       0.304447     0.00566893
   450        451       0.304183       0.304972     0.00554324
   460        461       0.304183       0.304732     0.00542299
   470        471       0.304183       0.305101     0.00530786
   480        481       0.304183       0.304509     0.00519751
   490        491       0.304183       0.304213     0.00509165
   500        501       0.303465       0.303687     0.00499002
   510        511       0.301779       0.301882     0.00489237
   520        521       0.301614       0.301621     0.00479846
   530        531       0.301614        0.30209      0.0047081
   540        541       0.301614       0.302704     0.00462107
   550        551       0.301614       0.301967     0.00453721
   560        561       0.301614       0.302526     0.00445633
   570        571       0.301614       0.302558     0.00437828
   580        581       0.301614       0.302929     0.00430293
   590        591       0.301614       0.302297     0.00423012
   600        601       0.301516       0.301516     0.00415973

                           Best        Current           Mean
Iteration   f-count         f(x)         f(x)         temperature
   610        611       0.301072       0.301322     0.00409165
   620        621       0.301072       0.301418     0.00402576
   630        631       0.300671       0.300742     0.00396197
   640        641       0.300565        0.30073     0.00390016
   650        651       0.299665       0.300159     0.00384025
   660        661       0.299665       0.300265     0.00378215
   670        671       0.299665       0.299878     0.00372578
   680        681       0.299665       0.300171     0.00367107
   690        691       0.299665        0.30029     0.00361795
   700        701       0.299665        0.30017     0.00356633
   710        711       0.299665       0.299711     0.00351617
   720        721       0.299665       0.300058     0.00346741
   730        731       0.299502       0.299502     0.00341997
   740        741       0.299224       0.299332     0.00337382
   750        751       0.299113        0.29968     0.00332889
   760        761       0.299113       0.299626     0.00328515
   770        771       0.298234       0.298234     0.00324254
   780        781       0.298234       0.298686     0.00320102
   790        791       0.297837       0.297837     0.00316056
   800        801       0.297837        0.29824      0.0031211
   810        811       0.297837       0.298486     0.00308261
   820        821       0.297837        0.29864     0.00304507
   830        831       0.297837       0.298272     0.00300842
   840        841       0.297621       0.297994     0.00297265
   850        851       0.297621       0.297654     0.00293772
   860        861       0.297183       0.297442      0.0029036
   870        871       0.297183       0.297468     0.00287026
   880        881       0.296838       0.297001     0.00283768
   890        891       0.296838       0.296873     0.00280584
   900        901       0.296401       0.296401     0.00277469

                           Best        Current           Mean
Iteration   f-count         f(x)         f(x)         temperature
   910        911       0.295981       0.296044     0.00274424
   920        921       0.295981        0.29615     0.00271444
   930        931       0.295891       0.295918     0.00268528
   940        941       0.295817        0.29598     0.00265675
   950        951       0.295449       0.295831     0.00262881
   960        961       0.295062       0.295062     0.00260146
   970        971       0.294771       0.294771     0.00257467
   980        981       0.294738       0.294947     0.00254842
   990        991       0.294686       0.294742      0.0025227
  1000       1001       0.294255       0.294294      0.0024975
  1010       1011       0.294248       0.294532      0.0024728
  1020       1021       0.294248       0.294583     0.00244858
  1030       1031       0.294248       0.294836     0.00242483
  1040       1041       0.294248       0.294423     0.00240154
  1050       1051       0.294247       0.294247     0.00237869
  1060       1061       0.294247       0.294433     0.00235627
  1070       1071       0.294203       0.294246     0.00233427
  1080       1081        0.29376       0.294035     0.00231267
  1090       1091        0.29376         0.2939     0.00229148
  1100       1101        0.29343        0.29343     0.00227066
  1110       1111       0.292732       0.292732     0.00225023
  1120       1121       0.292593       0.292929     0.00223015
  1130       1131       0.292438       0.292438     0.00221043
  1140       1141       0.292402        0.29265     0.00219106
  1150       1151       0.292402       0.292745     0.00217202
  1160       1161       0.292022       0.292022     0.00215332
  1170       1171       0.291842       0.292001     0.00213493
  1180       1181       0.291588       0.291821     0.00211685
  1190       1191       0.291576       0.291658     0.00209908
  1200       1201       0.291405       0.291405      0.0020816

                           Best        Current           Mean
Iteration   f-count         f(x)         f(x)         temperature
  1210       1211       0.290973       0.290991     0.00206441
  1220       1221       0.290794       0.290794      0.0020475
  1230       1231       0.290709       0.290876     0.00203087
  1240       1241       0.290598       0.290703      0.0020145
  1250       1251       0.290218       0.290329      0.0019984
  1260       1261       0.290162       0.290162     0.00198255
  1270       1271       0.290162       0.290262     0.00196696
  1280       1281       0.290162       0.290455      0.0019516
  1290       1291       0.290162       0.290548     0.00193648
  1300       1301       0.290162        0.29042      0.0019216
  1310       1311       0.290162       0.290438     0.00190694
  1320       1321       0.290162       0.290822     0.00189251
  1330       1331       0.290162       0.290554     0.00187829
  1340       1341       0.290162       0.290664     0.00186428
  1350       1351       0.290162       0.290423     0.00185048
  1360       1361       0.290157       0.290584     0.00183688
  1370       1371       0.290157       0.290272     0.00182349
  1380       1381       0.290157        0.29028     0.00181028
  1390       1391       0.290062       0.290162     0.00179727
  1400       1401       0.289505       0.289583     0.00178444
  1410       1411       0.289274       0.289274     0.00177179
  1420       1421       0.289026       0.289234     0.00175932
* 1421       1544       0.289026       0.289117       0.280101
  1430       1553       0.288675       0.294347       0.138791
  1440       1563       0.288675       0.294869      0.0891383
  1450       1573       0.288675       0.295085       0.065681
  1460       1583       0.288675       0.304048      0.0520047
  1470       1593       0.288675       0.299635      0.0430448
  1480       1603       0.288675       0.299713      0.0367195
  1490       1613       0.288675       0.303713      0.0320155
  1500       1623       0.288675       0.297954      0.0283801

                           Best        Current           Mean
Iteration   f-count         f(x)         f(x)         temperature
  1510       1633       0.288675       0.300414      0.0254863
  1520       1643       0.288675       0.305259      0.0231281
  1530       1653       0.288675       0.304304      0.0211694
  1540       1663       0.288675       0.305063      0.0195165
  1550       1673       0.288675       0.302073      0.0181031
  1560       1683       0.288675       0.303063      0.0168807
  1570       1693       0.288675       0.301778      0.0158129
  1580       1703       0.288675       0.301822      0.0148721
  1590       1713       0.288675        0.30286       0.014037
  1600       1723       0.288675       0.303196      0.0132907
  1610       1733       0.288675       0.304521      0.0126198
  1620       1743       0.288675       0.306977      0.0120133
  1630       1753       0.288675       0.306053      0.0114625
  1640       1763       0.288675       0.305425        0.01096
  1650       1773       0.288675       0.304733      0.0104996
  1660       1783       0.288675       0.304237      0.0100764
  1670       1793       0.288675       0.302215     0.00968602
  1680       1803       0.288675       0.301357     0.00932473
  1690       1813       0.288675        0.30049     0.00898943
  1700       1823       0.288675       0.299838      0.0086774
  1710       1833       0.288675       0.299096     0.00838631
  1720       1843       0.288675        0.29829     0.00811411
  1730       1853       0.288675       0.297823     0.00785903
  1740       1863       0.288675       0.297128      0.0076195
  1750       1873       0.288675       0.297804     0.00739414
  1760       1883       0.288675       0.297105     0.00718172
  1770       1893       0.288675       0.296628     0.00698117
  1780       1903       0.288675       0.296335     0.00679152
  1790       1913       0.288675       0.295709      0.0066119
  1800       1923       0.288675       0.295195     0.00644153

                           Best        Current           Mean
Iteration   f-count         f(x)         f(x)         temperature
  1810       1933       0.288675        0.29451     0.00627973
  1820       1943       0.288675       0.294213     0.00612585
  1830       1953       0.288675       0.294384     0.00597933
  1840       1963       0.288675       0.294452     0.00583966
  1850       1973       0.288675       0.294242     0.00570637
  1860       1983       0.288675       0.294115     0.00557902
  1870       1993       0.288675       0.293717     0.00545724
  1880       2003       0.288675       0.293305     0.00534066
  1890       2013       0.288675       0.292378     0.00522895
  1900       2023       0.288675       0.292514     0.00512182
  1910       2033       0.288675       0.292179       0.005019
  1920       2043       0.288675       0.292685     0.00492022
  1930       2053       0.288675       0.293768     0.00482525
  1940       2063       0.288675       0.293029     0.00473388
  1950       2073       0.288675       0.292954     0.00464591
  1960       2083       0.288675       0.292525     0.00456115
  1970       2093       0.288675       0.291905     0.00447942
  1980       2103       0.288675        0.29214     0.00440057
  1990       2113       0.288675       0.291574     0.00432445
  2000       2123       0.288675       0.291178     0.00425092
  2010       2133       0.288675        0.29055     0.00417985
  2020       2143       0.288675        0.29043     0.00411111
  2030       2153       0.288675       0.290756      0.0040446
  2040       2163       0.288675       0.291513     0.00398021
  2050       2173       0.288675       0.291132     0.00391783
  2060       2183       0.288675       0.291512     0.00385738
  2070       2193       0.288675       0.290423     0.00379877
  2080       2203       0.288675       0.291162     0.00374191
  2090       2213       0.288675       0.290355     0.00368673
  2100       2223       0.288675       0.290149     0.00363315

                           Best        Current           Mean
Iteration   f-count         f(x)         f(x)         temperature
  2110       2233       0.288675       0.290074     0.00358111
  2120       2243       0.288675       0.290599     0.00353053
  2130       2253       0.288675       0.290007     0.00348137
  2140       2263       0.288675        0.29014     0.00343355
  2150       2273       0.288675       0.289794     0.00338704
  2160       2283       0.288675       0.290045     0.00334176
  2170       2293       0.288675       0.289999     0.00329768
  2180       2303       0.288675       0.288875     0.00325475
  2190       2313       0.288453       0.288453     0.00321292
  2200       2323       0.288196       0.288948     0.00317215
  2210       2333       0.288196       0.288968      0.0031324
  2220       2343       0.288196       0.289271     0.00309364
  2230       2353       0.288196        0.28925     0.00305583
  2240       2363       0.288196       0.289581     0.00301893
  2250       2373       0.288196       0.290079     0.00298291
  2260       2383       0.288196        0.28953     0.00294773
  2270       2393       0.288196         0.2895     0.00291338
  2280       2403       0.288196       0.289619     0.00287982
  2290       2413       0.288196       0.289535     0.00284703
  2300       2423       0.288196       0.290267     0.00281497
  2310       2433       0.288196       0.289904     0.00278363
  2320       2443       0.288196       0.289102     0.00275297
  2330       2453       0.288196       0.289092     0.00272299
  2340       2463       0.288196       0.288416     0.00269365
  2350       2473       0.288196        0.28876     0.00266493
  2360       2483       0.288196       0.288541     0.00263683
  2370       2493       0.288196        0.28842     0.00260931
  2380       2503        0.28785        0.28785     0.00258235
  2390       2513       0.287205       0.287205     0.00255595
  2400       2523       0.286549       0.286549     0.00253008

                           Best        Current           Mean
Iteration   f-count         f(x)         f(x)         temperature
  2410       2533       0.286403       0.286503     0.00250474
  2420       2543       0.286046       0.286046     0.00247989
  2430       2553       0.285806       0.286185     0.00245553
  2440       2563       0.285773        0.28597     0.00243165
  2450       2573       0.285773       0.286467     0.00240822
  2460       2583       0.285773       0.286535     0.00238525
  2470       2593       0.285773       0.286348      0.0023627
  2480       2603       0.285773       0.286188     0.00234058
  2490       2613       0.285773       0.286225     0.00231887
  2500       2623       0.285492       0.285516     0.00229756
  2510       2633       0.285491       0.285527     0.00227664
  2520       2643       0.284271       0.284405     0.00225609
  2530       2653       0.284271       0.284828     0.00223592
  2540       2663       0.283579       0.283579      0.0022161
  2550       2673       0.282971       0.283187     0.00219663
  2560       2683       0.282891       0.282891     0.00217749
  2570       2693       0.282476       0.282572     0.00215869
  2580       2703       0.282476       0.282972     0.00214021
  2590       2713       0.282476       0.283065     0.00212204
  2600       2723       0.282476       0.282719     0.00210418
  2610       2733       0.282476       0.282816     0.00208662
  2620       2743       0.282476       0.283406     0.00206935
  2630       2753       0.282476       0.283073     0.00205236
  2640       2763       0.282476       0.282771     0.00203565
  2650       2773       0.282476       0.282602     0.00201921
  2660       2783       0.282476       0.282639     0.00200303
  2670       2793       0.282476       0.282775     0.00198711
  2680       2803       0.282476       0.282537     0.00197144
  2690       2813       0.282476        0.28248     0.00195601
  2700       2823       0.282378       0.282378     0.00194083

                           Best        Current           Mean
Iteration   f-count         f(x)         f(x)         temperature
  2710       2833       0.282182        0.28221     0.00192588
  2720       2843       0.282118       0.282541     0.00191115
  2730       2853       0.282118       0.282342     0.00189666
  2740       2863       0.282056       0.282057     0.00188237
  2750       2873       0.282056       0.282093     0.00186831
  2760       2883       0.281952        0.28245     0.00185445
  2770       2893       0.281952       0.282392     0.00184079
  2780       2903       0.281952       0.281991     0.00182734
  2790       2913       0.281776       0.281879     0.00181408
  2800       2923       0.281776        0.28192     0.00180101
  2810       2933       0.281776       0.282367     0.00178813
  2820       2943       0.281776       0.282224     0.00177543
* 2825       3070       0.281776       0.282099        0.26853
  2830       3075       0.281776       0.284611       0.173752
  2840       3085       0.281776       0.292134       0.102246
  2850       3095       0.281776       0.286931      0.0725107
  2860       3105       0.281776       0.288093       0.056189
  2870       3115       0.281776       0.290603      0.0458696
  2880       3125       0.281776       0.290473      0.0387543
  2890       3135       0.281776       0.295666      0.0335508
  2900       3145       0.281776       0.289872      0.0295796
  2910       3155       0.281776       0.287076      0.0264493
  2920       3165       0.281776         0.2903      0.0239182
  2930       3175       0.281776       0.292404      0.0218293
  2940       3185       0.281776       0.289515       0.020076
  2950       3195       0.281776       0.289482      0.0185835
  2960       3205       0.281776       0.287283      0.0172975
  2970       3215       0.281776        0.28772       0.016178
  2980       3225       0.281776       0.287266      0.0151947
  2990       3235       0.281776       0.287939       0.014324
  3000       3245       0.281776       0.288654      0.0135477

                           Best        Current           Mean
Iteration   f-count         f(x)         f(x)         temperature
  3010       3255       0.281776       0.288549      0.0128513
  3020       3265       0.281776       0.287163      0.0122229
  3030       3275       0.281776       0.287686      0.0116531
  3040       3285       0.281776       0.287305      0.0111341
  3050       3295       0.281776       0.287241      0.0106594
  3060       3305       0.281776       0.286765      0.0102234
  3070       3315       0.281776       0.287313     0.00982177
  3080       3325       0.281776       0.286213     0.00945048
  3090       3335       0.281776       0.286115     0.00910623
  3100       3345       0.281776       0.286133     0.00878619
  3110       3355       0.281776       0.284403     0.00848788
  3120       3365       0.281776       0.283779     0.00820916
  3130       3375       0.281776       0.283256     0.00794816
  3140       3385       0.281776       0.284074     0.00770325
  3150       3395       0.281776       0.283667     0.00747298
  3160       3405       0.281776       0.283571     0.00725607
  3170       3415       0.281776       0.283633     0.00705141
  3180       3425       0.281776       0.282867     0.00685797
  3190       3435       0.281776       0.282842     0.00667486
  3200       3445       0.281776       0.282866     0.00650128
  3210       3455       0.281776       0.283322      0.0063365
  3220       3465       0.281776       0.284083     0.00617986
  3230       3475       0.281776       0.283405     0.00603078
  3240       3485       0.281776       0.282814     0.00588872
  3250       3495       0.281776       0.282696      0.0057532
  3260       3505       0.281776       0.282567     0.00562378
  3270       3515       0.281776       0.283665     0.00550006
  3280       3525       0.281776       0.283747     0.00538166
  3290       3535       0.281776       0.282902     0.00526825
  3300       3545       0.281776       0.281952     0.00515952

                           Best        Current           Mean
Iteration   f-count         f(x)         f(x)         temperature
  3310       3555       0.281539       0.281583     0.00505519
  3320       3565       0.280763       0.280763       0.004955
  3330       3575       0.280468       0.280701      0.0048587
  3340       3585       0.279917       0.279917     0.00476607
  3350       3595       0.279701       0.279798     0.00467691
  3360       3605       0.279701       0.279959     0.00459102
  3370       3615       0.279701       0.279762     0.00450823
  3380       3625       0.279347       0.279563     0.00442837
  3390       3635       0.278395       0.278948     0.00435129
  3400       3645       0.278292       0.278292     0.00427685
  3410       3655       0.278197       0.278218     0.00420492
  3420       3665       0.277967       0.278293     0.00413536
  3430       3675       0.277706       0.277712     0.00406807
  3440       3685       0.277126       0.277126     0.00400293
  3450       3695       0.277126       0.277361     0.00393985
  3460       3705       0.277126       0.277137     0.00387872
  3470       3715       0.276899        0.27695     0.00381946
  3480       3725        0.27667       0.277172     0.00376199
  3490       3735        0.27667       0.276772     0.00370622
  3500       3745        0.27663       0.276646     0.00365207
  3510       3755        0.27663       0.276829     0.00359949
  3520       3765        0.27647       0.276617      0.0035484
  3530       3775       0.276365       0.276365     0.00349874
  3540       3785       0.276365       0.276551     0.00345045
  3550       3795       0.275983       0.275983     0.00340348
  3560       3805       0.275983       0.276503     0.00335777
  3570       3815       0.275983       0.276044     0.00331326
  3580       3825       0.275933       0.276647     0.00326993
  3590       3835       0.275933       0.276857     0.00322771
  3600       3845       0.275933       0.276852     0.00318657

                           Best        Current           Mean
Iteration   f-count         f(x)         f(x)         temperature
  3610       3855       0.275933       0.276555     0.00314646
  3620       3865       0.275933       0.277714     0.00310735
  3630       3875       0.275933       0.277494     0.00306921
  3640       3885       0.275933       0.277952     0.00303198
  3650       3895       0.275933       0.276899     0.00299565
  3660       3905       0.275933       0.276491     0.00296018
  3670       3915       0.275696       0.275747     0.00292554
  3680       3925       0.274907       0.274907      0.0028917
  3690       3935       0.274898       0.275034     0.00285864
  3700       3945       0.274898       0.275261     0.00282632
  3710       3955       0.274898       0.275598     0.00279472
  3720       3965       0.274898       0.275348     0.00276383
  3730       3975       0.274898       0.275357      0.0027336
  3740       3985       0.274898       0.275619     0.00270404
  3750       3995       0.274898       0.274964      0.0026751
  3760       4005       0.274041       0.274156     0.00264678
  3770       4015       0.273506       0.273597     0.00261905
  3780       4025       0.273083       0.273083      0.0025919
  3790       4035       0.273083       0.273263      0.0025653
  3800       4045       0.272536        0.27271     0.00253925
  3810       4055       0.272536       0.272641     0.00251372
Time limit exceeded: increase options.MaxTime.
