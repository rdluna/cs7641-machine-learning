import sys
import os
import time

sys.path.append("C:\ABAGAIL\ABAGAIL.jar")

import java.io.FileReader as FileReader
import java.io.File as File
import java.lang.String as String
import java.lang.StringBuffer as StringBuffer
import java.lang.Boolean as Boolean
import java.util.Random as Random

import dist.DiscreteDependencyTree as DiscreteDependencyTree
import dist.DiscreteUniformDistribution as DiscreteUniformDistribution
import dist.Distribution as Distribution
import opt.DiscreteChangeOneNeighbor as DiscreteChangeOneNeighbor
import opt.EvaluationFunction as EvaluationFunction
import opt.GenericHillClimbingProblem as GenericHillClimbingProblem
import opt.HillClimbingProblem as HillClimbingProblem
import opt.NeighborFunction as NeighborFunction
import opt.RandomizedHillClimbing as RandomizedHillClimbing
import opt.SimulatedAnnealing as SimulatedAnnealing
import opt.example.FourPeaksEvaluationFunction as FourPeaksEvaluationFunction
import opt.ga.CrossoverFunction as CrossoverFunction
import opt.ga.SingleCrossOver as SingleCrossOver
import opt.ga.DiscreteChangeOneMutation as DiscreteChangeOneMutation
import opt.ga.GenericGeneticAlgorithmProblem as GenericGeneticAlgorithmProblem
import opt.ga.GeneticAlgorithmProblem as GeneticAlgorithmProblem
import opt.ga.MutationFunction as MutationFunction
import opt.ga.StandardGeneticAlgorithm as StandardGeneticAlgorithm
import opt.ga.UniformCrossOver as UniformCrossOver
import opt.prob.GenericProbabilisticOptimizationProblem as GenericProbabilisticOptimizationProblem
import opt.prob.MIMIC as MIMIC
import opt.prob.ProbabilisticOptimizationProblem as ProbabilisticOptimizationProblem
import shared.FixedIterationTrainer as FixedIterationTrainer
import opt.example.KnapsackEvaluationFunction as KnapsackEvaluationFunction

from array import array
from time import clock
from itertools import product

"""
Commandline parameter(s):
   none
"""

N = [50,100,200,500]
maxIters = 5000
numTrials=5
outfile = 'C:/output/knapsack/KNAPSACK_@IT@_@ALG@_@N@.txt'

for n in N:
    # Random number generator */
    random = Random(n)
    # The number of items
    NUM_ITEMS = n
    # The number of copies each
    COPIES_EACH = 4
    # The maximum weight for a single element
    MAX_WEIGHT = 50
    # The maximum volume for a single element
    MAX_VALUE = 50
    # The volume of the knapsack 
    KNAPSACK_MAX_WEIGHT = MAX_WEIGHT * NUM_ITEMS * COPIES_EACH * .4
    
    print "MAX WEIGHT: " + str(KNAPSACK_MAX_WEIGHT)
    
    # create copies
    fill = [COPIES_EACH] * NUM_ITEMS
    copies = array('i', fill)
    
    # create weights and values
    fill = [0] * NUM_ITEMS
    weights = array('d', fill)
    values = array('d', fill)
    for i in range(0, NUM_ITEMS):
        weights[i] = random.nextDouble() * MAX_WEIGHT
        values[i] = random.nextDouble() * MAX_VALUE
    # create range
    fill = [COPIES_EACH + 1] * NUM_ITEMS
    ranges = array('i', fill)

     
    # RHC
    for t in range(numTrials):
        odd = DiscreteUniformDistribution(ranges)
        nf = DiscreteChangeOneNeighbor(ranges)
        mf = DiscreteChangeOneMutation(ranges)
        cf = UniformCrossOver()
        df = DiscreteDependencyTree(.1, ranges)
        ef = KnapsackEvaluationFunction(values, weights, KNAPSACK_MAX_WEIGHT, copies)
        hcp = GenericHillClimbingProblem(ef, odd, nf)
        gap = GenericGeneticAlgorithmProblem(ef, odd, mf, cf)
        pop = GenericProbabilisticOptimizationProblem(ef, odd, df)
        fname = outfile.replace('@ALG@','RHC').replace('@N@',str(t+1)).replace('@IT@',str(n))
        print fname
        with open(fname,'w') as f:
            f.write('it,funval,time,funcount,weight\n')
        rhc = RandomizedHillClimbing(hcp)
        fit = FixedIterationTrainer(rhc, 10)
        times =[0]
        for i in range(0,maxIters,10):
            start = clock()
            fit.train()
            elapsed = time.clock()-start
            times.append(times[-1]+elapsed)
            funcount = ef.funcount
            weight = ef.weight
            score = ef.value(rhc.getOptimal())
            funcount -= 1
            st = '{},{},{},{},{}\n'.format(i,score,times[-1],funcount,weight)
            #print st    
            with open(fname,'a') as f:
                f.write(st)
      
    # SA
    for t in range(numTrials):
        for CE in [0.15,0.35,0.55,0.75,0.95]:
            odd = DiscreteUniformDistribution(ranges)
            nf = DiscreteChangeOneNeighbor(ranges)
            mf = DiscreteChangeOneMutation(ranges)
            cf = UniformCrossOver()
            df = DiscreteDependencyTree(.1, ranges)
            ef = KnapsackEvaluationFunction(values, weights, KNAPSACK_MAX_WEIGHT, copies)
            hcp = GenericHillClimbingProblem(ef, odd, nf)
            gap = GenericGeneticAlgorithmProblem(ef, odd, mf, cf)
            pop = GenericProbabilisticOptimizationProblem(ef, odd, df)
            fname = outfile.replace('@ALG@','SA{}'.format(CE)).replace('@N@',str(t+1)).replace('@IT@',str(n))
            print fname
            with open(fname,'w') as f:
                f.write('it,funval,time,funcount,weight\n')
            sa = SimulatedAnnealing(1E10, CE, hcp)
            fit = FixedIterationTrainer(sa, 10)
            times =[0]
            for i in range(0,maxIters,10):
                start = clock()
                fit.train()
                elapsed = time.clock()-start
                times.append(times[-1]+elapsed)
                funcount = ef.funcount
                weight = ef.weight
                score = ef.value(sa.getOptimal())
                funcount -= 1
                st = '{},{},{},{},{}\n'.format(i,score,times[-1],funcount,weight)
                #print st
                with open(fname,'a') as f:
                    f.write(st)
      
    #GA
    for t in range(numTrials):
        for pop,mate,mutate in product([100],[50,30,10],[50,30,10]):
            odd = DiscreteUniformDistribution(ranges)
            nf = DiscreteChangeOneNeighbor(ranges)
            mf = DiscreteChangeOneMutation(ranges)
            cf = UniformCrossOver()
            df = DiscreteDependencyTree(.1, ranges)
            ef = KnapsackEvaluationFunction(values, weights, KNAPSACK_MAX_WEIGHT, copies)
            hcp = GenericHillClimbingProblem(ef, odd, nf)
            fname = outfile.replace('@ALG@','GA{}_{}_{}'.format(pop,mate,mutate)).replace('@N@',str(t+1)).replace('@IT@',str(n))
            print fname
            with open(fname,'w') as f:
                f.write('it,funval,time,funcount,weight\n')
            gap = GenericGeneticAlgorithmProblem(ef, odd, mf, cf)
            ga = StandardGeneticAlgorithm(pop, mate, mutate, gap)
            fit = FixedIterationTrainer(ga, 10)
            times =[0]
            for i in range(0,maxIters,10):
                start = clock()
                fit.train()
                elapsed = time.clock()-start
                times.append(times[-1]+elapsed)
                funcount = ef.funcount
                weight = ef.weight
                score = ef.value(ga.getOptimal())
                funcount -= 1
                st = '{},{},{},{},{}\n'.format(i,score,times[-1],funcount,weight)
                #print st
                with open(fname,'a') as f:
                    f.write(st)
      
    #MIMIC
    for t in range(numTrials):
        for samples,keep,m in product([100],[50],[0.1,0.3,0.5,0.7,0.9]):
            odd = DiscreteUniformDistribution(ranges)
            nf = DiscreteChangeOneNeighbor(ranges)
            mf = DiscreteChangeOneMutation(ranges)
            cf = UniformCrossOver()
            df = DiscreteDependencyTree(.1, ranges)
            ef = KnapsackEvaluationFunction(values, weights, KNAPSACK_MAX_WEIGHT, copies)
            hcp = GenericHillClimbingProblem(ef, odd, nf)
            gap = GenericGeneticAlgorithmProblem(ef, odd, mf, cf)
            pop = GenericProbabilisticOptimizationProblem(ef, odd, df)
            fname = outfile.replace('@ALG@','MIMIC{}_{}_{}'.format(samples,keep,m)).replace('@N@',str(t+1)).replace('@IT@',str(n))
            print fname
            with open(fname,'w') as f:
                f.write('it,funval,time,funcount,weight\n')
            gap = GenericGeneticAlgorithmProblem(ef, odd, mf, cf)
            df = DiscreteDependencyTree(m, ranges)
            pop = GenericProbabilisticOptimizationProblem(ef, odd, df)
            mimic = MIMIC(samples, keep, pop)
            fit = FixedIterationTrainer(mimic, 10)
            times =[0]
            for i in range(0,maxIters,10):
                start = clock()
                fit.train()
                elapsed = time.clock()-start
                times.append(times[-1]+elapsed)
                funcount = ef.funcount
                weight = ef.weight
                score = ef.value(mimic.getOptimal())
                funcount -= 1
                st = '{},{},{},{},{}\n'.format(i,score,times[-1],funcount,weight)
                #print st
                with open(fname,'a') as f:
                    f.write(st)
