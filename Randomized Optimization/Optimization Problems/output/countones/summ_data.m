clc
clear all
close force all

N = [50 100 200 500];
alg = {'RHC','SA','GA','MIMIC'};
numTrials = 5;
SA = [0.15, 0.35, 0.55, 0.75, 0.95];
GA = {'100_50_50','100_50_30','100_50_10',...
    '100_30_50','100_30_30','100_30_10',...
    '100_10_50','100_10_30','100_10_10'};
MIMIC = {'100_50_0.1',...
    '100_50_0.3',...
    '100_50_0.5',...
    '100_50_0.7',...
    '100_50_0.9'};

data_RHC = table;
data_SA = table;
data_GA = table;
data_MIMIC = table;

for n = 1:numel(N)
    dir_name = strcat('N',num2str(N(n)));
    if ~exist(dir_name,'dir')
        mkdir(dir_name)
    end
    for a = 1:numel(alg)
        files = dir(['COUNTONES_' num2str(N(n)) '_' alg{a} '*.txt']);
        files = cellstr(char(files.name));
        if strcmp(alg{a},'RHC')
            for t = 1:numTrials
                data = readtable(files{t});
                data.Trial = ones(size(data,1),1)*t;
                data.N = ones(size(data,1),1)*N(n);
                data_RHC = vertcat(data_RHC,data);
            end
            cd(dir_name)
            cd ..
        elseif strcmp(alg{a},'SA')
            for sa = 1:numel(SA)
                subfiles_idx = ~cellfun(@isempty,regexp(files,strcat('_SA',num2str(SA(sa)),'_')));
                subfiles = files(subfiles_idx);
                for t = 1:numTrials
                    data = readtable(subfiles{t});
                    data.Trial = ones(size(data,1),1)*t;
                    data.N = ones(size(data,1),1)*N(n);
                    data.CE = ones(size(data,1),1)*SA(sa);
                    data_SA = vertcat(data_SA,data);
                end
                cd(dir_name)
                cd ..
            end
        elseif strcmp(alg{a},'GA')
            for ga = 1:numel(GA)
                subfiles_idx = ~cellfun(@isempty,regexp(files,GA{ga}));
                subfiles = files(subfiles_idx);
                for t = 1:numTrials
                    data = readtable(subfiles{t});
                    data.Trial = ones(size(data,1),1)*t;
                    data.N = ones(size(data,1),1)*N(n);
                    locstring = GA{ga};
                    data.mate = ones(size(data,1),1)*str2double(locstring(5:6));
                    data.mutate = ones(size(data,1),1)*str2double(locstring(end-1:end));
                    data_GA = vertcat(data_GA,data);
                end
                cd(dir_name)
                cd ..
            end
        elseif strcmp(alg{a},'MIMIC')
            for mimic = 1:numel(MIMIC)
                subfiles_idx = ~cellfun(@isempty,regexp(files,MIMIC{mimic}));
                subfiles = files(subfiles_idx);
                for t = 1:numTrials
                    data = readtable(subfiles{t});
                    data.Trial = ones(size(data,1),1)*t;
                    data.N = ones(size(data,1),1)*N(n);
                    locstring = MIMIC{mimic};
                    data.keep = ones(size(data,1),1)*str2double(locstring(5:6));
                    data.m = ones(size(data,1),1)*str2double(locstring(end-2:end));
                    data_MIMIC = vertcat(data_MIMIC,data);
                end
                cd(dir_name)
                cd ..
            end
        end
    end
end

if ~exist('OUT_DATA','dir')
    mkdir('OUT_DATA')
end
cd('OUT_DATA')

writetable(data_RHC,'RHC_data.csv')
writetable(data_SA,'SA_data.csv')
writetable(data_GA,'GA_data.csv')
writetable(data_MIMIC,'MIMIC_data.csv')
