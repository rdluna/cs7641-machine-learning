"""
Ownership of the following code and report developed as a result of assigned institutional effort,
an assignment of the CS 7641 Machine Learning course shall reside with GT and the instructors
of this class. If the document is released into the public domain it will violate the GT Honor Code.
"""

CODE PARTIALLY BASED ON THESE SOURCES:

UC Berkeley Deep RL Bootcamp Lab 1: 
https://drive.google.com/file/d/0B1BwaUH2mk-EdXI5dzlLbWl5R0E/view
Code by Moustafa Alzantot from UCLA in Medium:
https://medium.com/@m.alzantot/deep-reinforcement-learning-demysitifed-episode-2-policy-iteration-value-iteration-and-q-978f9e89ddaa

SOFTWARE REQUIREMENTS:
For the training of the algorithms:
- Python 3.5.1
	- PIL (Pillow) 4.0.0
	- gym 0.9.4
	- imageio 2.2.0
	- matplotlib 2.1.0
	- numpy 1.13.3
	- pandas 0.21.0
For plotting and report generation:
- R Studio with R version 3.4.1 (2017-06-30)

GYM ENVIRONMENTS
The 2 problems used in this Assignment are the 4x4 and 8x8 FrozenLake environments (FrozenLake-v0 and FrozenLake8x8-v0, respectively).
The gym file frozen_lake.py in "C:\Program Files\Python35\Lib\site-packages\gym\envs\toy_text\frozen_lake.py" (or equivalent) has been modified (the rendering section).
The modified copy of this file is provided in the ZIP file.

DIRECTORIES:
There are 2 main directories, one for each problem type.
There are 3 subdirectories, each for a type of solution (Value Iteration, Policy Iteration and Q-Learning).
The Q-Learning directory contains subdirectories for the 2 types of Q-Learning implemented.

RUN INSTRUCTIONS:
- To create the directories structure use the SetupDirs script
- To run Value and Policy iteration use the script Value-Policy_Iteration, changing the env_name variable to the desired problem.
- To run Q-Learning use the script Q-Learning, changing the env_name variable to the desired problem.