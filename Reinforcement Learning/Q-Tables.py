from PIL import Image
import matplotlib.pyplot as plt
import numpy as np
import imageio

def plot_table(data, width, nS):
    if nS == 4:
        height = 5
    elif nS == 8:
        height = 6
    plt.figure(figsize=(width, height), facecolor='white')
    plt.tight_layout()
    plt.imshow(data, cmap="gray", clim=(0, 1), interpolation='none', aspect='auto')
    ax = plt.gca()
    ax.set_xticks(np.arange(4) - .5)
    ax.set_yticks(np.arange(nS ** 2) - .5)
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    if nS == 4:
        plt.grid(color='b', lw=2, ls='-')
    elif nS == 8:
        plt.grid(color='b', lw=1, ls='-')
    if alg == '/greedy':
        plt.title(r'$\gamma =$' + str(gamma) + '\n' + r'$\epsilon =$' + str(epsilon), fontsize=16)
    elif alg == '/non-greedy':
        plt.title(r'$\gamma =$' + str(gamma), fontsize=16)
    canvas = plt.get_current_fig_manager().canvas
    canvas.draw()
    pil_image = Image.frombytes('RGB', canvas.get_width_height(), canvas.tostring_rgb())
    plt.close()
    return pil_image

wdir = "D:/Users/212415648/Documents/EDUCATION/Masters/Machine Learning/Assignment 4/"
problems = ["4x4/", "8x8/"]
algs = ["/greedy", "/non-greedy"]
epsilon_array = np.append(np.linspace(0.05, 0.15, 5), [0.25, 0.375, 0.5, 0.75, 0.90])
gamma_array = [0.95, 0.96, 0.97, 0.98, 0.99]

for problem in problems:
    if problem == "4x4/":
        width = 1
        nS = 4
    elif problem == "8x8/":
        width = 1
        nS = 8
    for alg in algs:
        if alg == '/greedy':
            for gamma in gamma_array:
                images = []
                for idx, epsilon in enumerate(epsilon_array):
                    data = np.genfromtxt(wdir + problem + 'Q-Learning' + alg + '/' + str(gamma) + '_' + str(idx) + '_Q.txt', delimiter=',')
                    data_img = plot_table(data, width, nS)
                    data_img.save(wdir + problem + 'Q-Learning' + alg + '/' + str(gamma) + '_' + str(idx) + '_Q.png')
                    images.append(np.array(data_img))
                imageio.mimsave(wdir + problem + 'Q-Learning' + alg + '/' + 'Q_tables_' + str(gamma) + '.gif', images, duration=0.25)
        elif alg == '/non-greedy':
            images = []
            for gamma in gamma_array:
                data = np.genfromtxt(wdir + problem + 'Q-Learning' + alg + '/' + str(gamma) + '_0_Q.txt', delimiter=',')
                data_img = plot_table(data, width, nS)
                data_img.save(wdir + problem + 'Q-Learning' + alg + '/' + str(gamma) + '_0_Q.png')
                images.append(np.array(data_img))
            imageio.mimsave(wdir + problem + 'Q-Learning' + alg + '/' + 'Q_tables.gif', images, duration=0.25)