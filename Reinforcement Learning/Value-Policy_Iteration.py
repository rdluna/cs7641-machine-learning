"""
Ownership of the following code developed as a result of assigned institutional effort,
an assignment of the CS 7641 Machine Learning course shall reside with GT and the instructors
of this class. If the document is released into the public domain it will violate the GT Honor Code.
"""

import gym
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from PIL import Image
import imageio
import time
import re

wdir = "D:/Users/212415648/Documents/EDUCATION/Masters/Machine Learning/Assignment 4/"
np.set_printoptions(precision=3)
plt.close("all")

env_name = 'FrozenLake8x8-v0'
env = gym.make(env_name)
print(env.unwrapped.__doc__)

if env_name == 'FrozenLake-v0':
    wdir = wdir + "4x4/"
elif env_name == 'FrozenLake8x8-v0':
    wdir = wdir + "8x8/"

class MDP(object):
    def __init__(self, P, nS, nA, desc=None):
        self.P = P  # state transition and reward probabilities, explained below
        self.nS = nS  # number of states
        self.nA = nA  # number of actions
        self.desc = desc  # 2D array specifying what each grid cell means (used for plotting)

# VALUE ITERATION FUNCTIONS

def value_iteration(mdp, gamma, maxIt, tol=1e-10, it_print=print):
    """
    Inputs:
        mdp: MDP instance
        gamma: discount factor
        maxIt: max number of iterations
        tol: tolerance for the innovations
    Outputs:
        value_functions
        policies
        values_diff: innovation between iterations
        actions_diff: change in actions between iterations
        mean_values: mean values for the grid
        it_times: time for each iteration
    """
    it_print("Iteration |  Innovation  | Diff Actions | E[values] ")
    it_print("----------+--------------+--------------+-----------")
    value_functions = [np.zeros(mdp.nS)]  # initial value function values
    policies = []
    values_diff = []
    actions_diff = []
    mean_values = []
    it_times = []
    for it in range(maxIt):
        it_tic = time.clock()
        old_policy = policies[-1] if len(policies) > 0 else None
        previous_values = value_functions[-1]
        values = np.copy(previous_values)
        policy = np.zeros(mdp.nS)
        for state in range(mdp.nS):
            value, action = values[state], policy[state]
            for action_idx in range(mdp.nA):
                action_value = np.sum([probability * (reward + gamma * previous_values[next_state]) for probability, next_state, reward in mdp.P[state][action_idx]])
                if action_value > value:
                    value, action = action_value, action_idx
            values[state], policy[state] = value, action
        it_toc = time.clock()
        it_times.append(it_toc - it_tic)
        values_diff.append(np.sum(np.fabs(values - previous_values)))
        actions_diff.append("N/A" if old_policy is None else (policy != old_policy).sum())
        value_functions.append(values)
        mean_values.append(np.mean(values))
        policies.append(policy)
        it_print(
            "%4i      | %6.5f      | %4s         | %5.5f" % (it+1, values_diff[-1], actions_diff[-1], mean_values[-1]))
        if values_diff[-1] <= tol:
            print('Value iteration converged at iteration %i \n' % int(it))
            return value_functions, policies, values_diff, actions_diff, mean_values, it_times
    print('Reached maximum number of iterations \n')
    return value_functions, policies, values_diff, actions_diff, mean_values, it_times


# POLICY ITERATION FUNCTIONS

# State value function V^policy(s)
def compute_vpi(policy, mdp, gamma):
    # use policy[state] to access the action that's prescribed by this policy
    A, b = np.zeros((mdp.nS, mdp.nS)), np.zeros(mdp.nS)
    for s in range(mdp.nS):
        for p, ns, r in mdp.P[s][policy[s]]:
            b[s] += p*r
            A[s, ns] += p*gamma
    A -= np.eye(mdp.nS)
    Vpi = -np.linalg.solve(A, b).T
    return Vpi

# State-action value function Q^policy(s,a)
def compute_qpi(vpi, mdp, gamma):
    Qpi = np.zeros([mdp.nS, mdp.nA])
    for s in range(mdp.nS):
        for a in range(mdp.nA):
            for p, ns, r in mdp.P[s][a]:
                Qpi[s,a] += p*(r + gamma*vpi[ns])
    return Qpi


def policy_iteration(mdp, gamma, maxIt, tol=1e-10, it_print=print):
    values = [np.zeros(mdp.nS)]
    policies = []
    values_diff = []
    actions_diff = []
    mean_values = []
    it_times = []
    previous_policy = np.zeros(mdp.nS, dtype='int')
    policies.append(previous_policy)
    it_print("Iteration |  Innovation  | Diff Actions | E[values] ")
    it_print("----------+--------------+--------------+-----------")
    for it in range(maxIt):
        previous_values = values[-1]
        it_tic = time.clock()
        vpi = compute_vpi(previous_policy, mdp, gamma)
        qpi = compute_qpi(vpi, mdp, gamma)
        policy = qpi.argmax(axis=1)
        it_toc = time.clock()
        it_times.append(it_toc - it_tic)
        values_diff.append(np.sum(np.fabs(vpi - previous_values)))
        actions_diff.append((policy != previous_policy).sum())
        mean_values.append(np.mean(vpi))
        it_print("%4i      | %6.5f      | %4s         | %5.5f" % (
            it + 1, values_diff[-1], actions_diff[-1], mean_values[-1]))
        values.append(vpi)
        policies.append(policy)
        previous_policy = policy
        if values_diff[-1] <= tol and it > 0:
            print('Policy iteration converged at iteration %i \n' % int(it + 1))
            return values, policies, values_diff, actions_diff, mean_values, it_times
    print('Reached maximum number of iterations \n')
    return values, policies, values_diff, actions_diff, mean_values, it_times


# PLOTTING FUNCTIONS

# Function to plot the grid with policy and values
def plot_vp(values, policies, plot_it, dir, env):
    nS = int(np.sqrt(env.unwrapped.nS))
    images = []
    it = 0
    for (values, policy) in zip(values, policies):
        plt.figure(figsize=(3, 3))
        plt.imshow(values.reshape(nS, nS), cmap='gray', interpolation='none', clim=(0, 1))
        ax = plt.gca()
        ax.set_xticks(np.arange(nS) - .5)
        ax.set_yticks(np.arange(nS) - .5)
        ax.set_xticklabels([])
        ax.set_yticklabels([])
        Y, X = np.mgrid[0:nS, 0:nS]
        a2uv = {0: (-1, 0), 1: (0, -1), 2: (1, 0), 3: (-1, 0)}
        Pi = policy.reshape(nS, nS)
        for y in range(nS):
            for x in range(nS):
                a = Pi[y, x]
                u, v = a2uv[a]
                #plt.scatter(x, y, marker='X')
                plt.arrow(x, y, u * .3, -v * .3, color='m', head_width=0.1, head_length=0.1)
                plt.text(x, y, str(env.unwrapped.desc[y, x].item().decode()),
                         color='g', size=12, verticalalignment='center',
                         horizontalalignment='center', fontweight='bold')
        plt.grid(color='b', lw=2, ls='-')
        plt.title("Iteration " + str(it + 1))
        canvas = plt.get_current_fig_manager().canvas
        canvas.draw()
        pil_image = Image.frombytes('RGB', canvas.get_width_height(), canvas.tostring_rgb())
        images.append(np.array(pil_image))
        if it in plot_it:
            pil_image.save(wdir + dir + "plot_" + str(it + 1) + ".png")
        plt.close()
        it += 1
    imageio.mimsave(wdir + dir + "movie.gif", images, duration=0.1)


# EVALUATION FUNCTIONS

def run_episode(env, policy, gamma, render=False):
    """ Evaluates policy by using it to run an episode and finding its
    total reward.

    args:
    env: gym environment.
    policy: the policy to be used.
    gamma: discount factor.
    render: boolean to turn rendering on/off.

    returns:
    total reward: real value of the total reward recieved by agent under policy.
    """
    obs = env.reset()
    total_reward = 0
    step_idx = 0
    while True:
        if render:
            env.render()
        obs, reward, done, _ = env.step(int(policy[obs]))
        total_reward += (gamma ** step_idx * reward)
        step_idx += 1
        if done:
            break
    return total_reward


def evaluate_policy(env, policy, n, gamma):
    """ Evaluates a policy by running it n times.
    returns:
    average total reward
    """
    scores = [run_episode(env, policy, gamma=gamma, render=False) for _ in range(n)]
    return np.mean(scores)


def evaluate_policies(env, policies, n, gamma):
    """ Evaluates a policy by running it n times.
    returns:
    average total reward
    """
    scores = []
    seeds = range(10)
    for policy in policies:
        policy_scores = []
        for seed in seeds:
            env.seed(seed)
            seed_scores = [run_episode(env, policy, gamma=gamma, render=False) for _ in range(n)]
            policy_scores.append(np.mean(seed_scores))
        scores.append(np.mean(policy_scores))
    return scores


def plot_episode(env, policy, gamma, ep_name, seed):
    """ Evaluates policy by using it to run an episode and finding its
    total reward.
    args:
    env: gym environment.
    policy: the policy to be used.
    gamma: discount factor.
    render: boolean to turn rendering on/off.
    returns:
    total reward: real value of the total reward recieved by agent under policy.
    """
    win = False
    lose = False
    obs = env.reset()
    env.seed(seed)
    total_reward = 0
    step_idx = 0
    images = []
    scenario_array = np.array(env.unwrapped.desc, dtype='str')
    scenario_image = np.ones(scenario_array.shape)
    scenario_image[scenario_array == 'H'] = 0
    while True:
        nS = int(np.sqrt(env.unwrapped.nS))
        render = env.render()
        render = np.array(list(re.sub(r'[^A-Z]\[41m[S,F,H,G][^A-Z]\[0m', 'C', render).replace("\n", ""))).reshape((nS, nS))
        plt.figure(figsize=(3, 3))
        plt.imshow(scenario_image, cmap='gray', interpolation='none', clim=(0, 1))
        plt.title("Step " + str(int(step_idx+1)))
        ax = plt.gca()
        ax.set_xticks(np.arange(nS) - .5)
        ax.set_yticks(np.arange(nS) - .5)
        ax.set_xticklabels([])
        ax.set_yticklabels([])
        Y, X = np.mgrid[0:nS, 0:nS]
        a2uv = {0: (-1, 0), 1: (0, -1), 2: (1, 0), 3: (-1, 0)}
        Pi = policy.reshape(nS, nS)
        for i, y in enumerate(range(nS)):
            for j, x in enumerate(range(nS)):
                a = Pi[y, x]
                u, v = a2uv[a]
                if scenario_array[i, j] in ['S', 'G']:
                    plt.text(x, y, str(env.unwrapped.desc[y, x].item().decode()),
                             color='g', size=12, verticalalignment='center',
                             horizontalalignment='center', fontweight='bold')
                if render[i, j] == 'C':
                    if scenario_array[i, j] == 'G':
                        plt.title('Reached the goal!')
                        win = True
                        plt.scatter(x, y, marker='o', s=200, c="green")
                    elif scenario_array[i, j] == 'H':
                        plt.title('Fell in a hole!')
                        lose = True
                        plt.scatter(x, y, marker='o', s=200, c="red")
                    else:
                        plt.scatter(x, y, marker='o', s=200, c="blue")
                        if scenario_image[i, j] > 0.25:
                            scenario_image[i, j] -= 0.0625
                plt.arrow(x, y, u * .3, -v * .3, color='m', head_width=0.1, head_length=0.1)
        plt.grid(color='b', lw=2, ls='-')
        canvas = plt.get_current_fig_manager().canvas
        canvas.draw()
        pil_image = Image.frombytes('RGB', canvas.get_width_height(), canvas.tostring_rgb())
        images.append(np.array(pil_image))
        plt.close()
        obs, reward, done, _ = env.step(int(policy[obs]))
        total_reward += (gamma ** step_idx * reward)
        step_idx += 1
        if done:
            if win or lose:
                for i in range(10):
                    images.append(images[-1])
            imageio.mimsave(wdir + ep_name + ".gif", images, duration=0.25)
            break


mdp = MDP({s: {a: [tup[:3] for tup in tups] for (a, tups) in a2d.items()} for (s, a2d) in env.unwrapped.P.items()},
          env.unwrapped.nS, env.unwrapped.nA, env.unwrapped.desc)

gamma = 0.99
max_iterations = 5000
n_episodes = 100

VI_values, VI_policies, VI_values_diff, VI_actions_diff, VI_mean_values, VI_times = value_iteration(mdp, gamma=gamma,
                                                                                                    maxIt=max_iterations)
VI_policies_eval = [VI_policies[i] for i in np.linspace(0, len(VI_policies)-1, 20, dtype='int')]
VI_scores = evaluate_policies(env, VI_policies_eval, n_episodes, gamma)
VI_scores_dataframe = pd.DataFrame(VI_scores, columns=["Score"])
VI_scores_dataframe['Iteration'] = np.linspace(0, len(VI_policies)-1, 20, dtype='int')
VI_scores_dataframe.to_csv(wdir + "Value Iteration/VI_scores.csv", sep=",", index=None)

PI_values, PI_policies, PI_values_diff, PI_actions_diff, PI_mean_values, PI_times = policy_iteration(mdp, gamma=gamma,
                                                                                                     maxIt=max_iterations)
PI_scores = evaluate_policies(env, PI_policies, n_episodes, gamma)
PI_scores_dataframe = pd.DataFrame(PI_scores, columns=["Score"])
PI_scores_dataframe['Iteration'] = range(len(PI_policies))
PI_scores_dataframe.to_csv(wdir + "Policy Iteration/PI_scores.csv", sep=",", index=None)

VI_dataframe = pd.DataFrame()
VI_dataframe['Iteration'] = range(len(VI_mean_values))
VI_dataframe['Innovation'] = VI_values_diff
VI_dataframe['Mean Value'] = VI_mean_values
VI_dataframe['Action Changes'] = VI_actions_diff
VI_dataframe['Iteration Time'] = VI_times
VI_dataframe.to_csv(wdir + "Value Iteration/VI_iterations.csv", sep=",", index=None)
np.savetxt(wdir + "Value Iteration/VI_values.csv", np.array(VI_values), delimiter=",", fmt="%1.8f")
np.savetxt(wdir + "Value Iteration/VI_policies.csv", np.array(VI_policies), delimiter=",", fmt="%i")

PI_dataframe = pd.DataFrame()
PI_dataframe['Iteration'] = range(len(PI_mean_values))
PI_dataframe['Innovation'] = PI_values_diff
PI_dataframe['Mean Value'] = PI_mean_values
PI_dataframe['Action Changes'] = PI_actions_diff
PI_dataframe['Iteration Time'] = PI_times
PI_dataframe.to_csv(wdir + "Policy Iteration/PI_iterations.csv", sep=",", index=None)
np.savetxt(wdir + "Policy Iteration/PI_values.csv", np.array(PI_values), delimiter=",", fmt="%1.8f")
np.savetxt(wdir + "Policy Iteration/PI_policies.csv", np.array(PI_policies), delimiter=",", fmt="%i")

VI_plot_it = [0, 4, 9, 19, 49, 99, 199, 299, 399, 499, 599]
VI_dir = "Value Iteration/"
plot_vp(VI_values, VI_policies, VI_plot_it, VI_dir, env)

PI_plot_it = range(len(PI_values))
PI_dir = "Policy Iteration/"
plot_vp(PI_values, PI_policies, PI_plot_it, PI_dir, env)

plot_episode(env, PI_policies[-1], gamma, "Policy Iteration/episode", seed=1)
plot_episode(env, VI_policies[-1], gamma, "Value Iteration/episode", seed=1)

print("Done")

"""
Ownership of the following code developed as a result of assigned institutional effort,
an assignment of the CS 7641 Machine Learning course shall reside with GT and the instructors
of this class. If the document is released into the public domain it will violate the GT Honor Code.
"""