"""
Ownership of the following code developed as a result of assigned institutional effort,
an assignment of the CS 7641 Machine Learning course shall reside with GT and the instructors
of this class. If the document is released into the public domain it will violate the GT Honor Code.
"""

import gym
import os
import numpy as np
import re
import matplotlib.pyplot as plt
from PIL import Image
import imageio
import random

wwdir = "D:/Users/212415648/Documents/EDUCATION/Masters/Machine Learning/Assignment 4/"


def plot_episode(env, step_idx, scenario_image, scenario_array):
    """ Evaluates policy by using it to run an episode and finding its
    total reward.
    args:
    env: gym environment.
    policy: the policy to be used.
    gamma: gamma factor.
    render: boolean to turn rendering on/off.
    returns:
    total reward: real value of the total reward recieved by agent under policy.
    """
    status = "continue"

    nS = int(np.sqrt(env.unwrapped.nS))
    render = env.render()
    render = np.array(list(re.sub(r'[^A-Z]\[41m[S,F,H,G][^A-Z]\[0m', 'C', render).replace("\n", ""))).reshape((nS, nS))

    plt.figure(figsize=(3, 3))
    plt.imshow(scenario_image, cmap='gray', interpolation='none', clim=(0, 1))
    plt.title("Episode " + str(int(step_idx+1)))
    ax = plt.gca()
    ax.set_xticks(np.arange(nS) - .5)
    ax.set_yticks(np.arange(nS) - .5)
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    Y, X = np.mgrid[0:nS, 0:nS]
    a2uv = {0: (-1, 0), 1: (0, -1), 2: (1, 0), 3: (-1, 0)}
    for i, y in enumerate(range(nS)):
        for j, x in enumerate(range(nS)):
            if scenario_array[i, j] in ['S', 'G']:
                plt.text(x, y, str(env.unwrapped.desc[y, x].item().decode()),
                         color='g', size=12, verticalalignment='center',
                         horizontalalignment='center', fontweight='bold')
            if render[i, j] == 'C':
                if scenario_array[i, j] == 'G':
                    plt.title('Reached the goal!')
                    plt.scatter(x, y, marker='o', s=200, c="green")
                    status = "win"
                elif scenario_array[i, j] == 'H':
                    plt.title('Fell in a hole!')
                    plt.scatter(x, y, marker='o', s=200, c="red")
                    status = "lose"
                else:
                    plt.scatter(x, y, marker='o', s=200, c="blue")
                    if scenario_image[i, j] > 0.25:
                        scenario_image[i, j] -= 0.0625
    plt.grid(color='b', lw=2, ls='-')
    canvas = plt.get_current_fig_manager().canvas
    canvas.draw()
    pil_image = Image.frombytes('RGB', canvas.get_width_height(), canvas.tostring_rgb())
    plt.close()
    return np.array(pil_image), status, scenario_image


def qUpdate(q_function, state_old, state_new, action, reward, learn_rate, gamma):
    q_function[state_old, action] += \
        learn_rate * (reward + gamma * q_function[state_new, :].max() - q_function[state_old, action])
    return q_function


def epsGreedyAction(q_function, state, epsilon):
    greedy_action = q_function[state, :].argmax()
    if random.random() < epsilon:
        action = random.sample(action_space, 1)[0]
    else:
        action = greedy_action
    return action


# Environment initialization
env_name = 'FrozenLake-v0'
env = gym.make(env_name)

if env_name == 'FrozenLake-v0':
    wwdir = wwdir + "4x4/"
elif env_name == 'FrozenLake8x8-v0':
    wwdir = wwdir + "8x8/"

# Parameters
init_alpha = 0.85
min_alpha = 0.05

episodes = 10000

gamma_array = [0.95, 0.96, 0.97, 0.98, 0.99]

action_space = range(0, env.action_space.n)

max_steps = env.spec.tags.get('wrapper_config.TimeLimit.max_episode_steps')

learners = ['greedy/', 'non-greedy/']


# Episodes
for learner in learners:
    wdir = wwdir + 'Q-Learning/' + learner
    if learner == 'greedy/':
        epsilon_array = np.append(np.linspace(0.05, 0.15, 5), [0.25, 0.375, 0.5, 0.75, 0.90])
    else:
        epsilon_array = [0]
    for gamma in gamma_array:
        episode_rewards = np.zeros((episodes, len(epsilon_array)))
        episode_iterations = np.zeros((episodes, len(epsilon_array)))
        episode_distance = np.zeros((episodes, len(epsilon_array)))
        actions_0 = 0
        actions_1 = 0
        actions_2 = 0
        actions_3 = 0
        for idx, epsilon in enumerate(epsilon_array):
            Q = np.zeros((env.observation_space.n, env.action_space.n))
            for episode in range(episodes):
                images = []
                it = 0

                # Refresh state
                state = env.reset()
                episode_reward = 0
                done = False

                scenario_array = np.array(env.unwrapped.desc, dtype='str')
                scenario_image = np.ones(scenario_array.shape)
                scenario_image[scenario_array == 'H'] = 0

                alpha = max(min_alpha, init_alpha * (0.85 ** (episode // 500)))

                # Run episode
                for i in range(max_steps):
                    if done:
                        nS = int(np.sqrt(env.unwrapped.nS))
                        render = env.render()
                        render = np.array(list(re.sub(r'[^A-Z]\[41m[S,F,H,G][^A-Z]\[0m', 'C', render).replace("\n", ""))).reshape((nS, nS))
                        last_position = render == 'C'
                        episode_distance[episode, idx] = abs(nS - np.where(last_position)[0][0]) + abs(nS - np.where(last_position)[1][0])
                        break
                    current = state
                    if learner == 'greedy/':
                        action = epsGreedyAction(Q, current, epsilon)
                    else:
                        action = np.argmax(Q[current, :] + np.random.randn(1, env.action_space.n) * (1 / float(episode + 1)))
                    if action == 0:
                        actions_0 += 1
                    if action == 1:
                        actions_1 += 1
                    if action == 2:
                        actions_2 += 1
                    if action == 3:
                        actions_3 += 1
                    state, reward, done, info = env.step(action)
                    if reward == 0:
                        reward = -0.05
                    episode_reward += reward
                    if learner == 'greedy/':
                        Q = qUpdate(Q, current, state, action, reward, alpha, gamma)
                    else:
                        Q[current, action] += alpha * (reward + gamma * np.max(Q[state, :]) - Q[current, action])
                    if (episode + 1) % 1000 == 0 or episode == 0:
                        if not os.path.exists(wdir + str(episode+1)):
                            os.makedirs(wdir + str(episode+1))
                        env_img, status, scenario_image = plot_episode(env, episode, scenario_image, scenario_array)
                        if status in ["win", "lose"]:
                            for _ in range(9):
                                images.append(env_img)
                        images.append(env_img)
                    it += 1
                if (episode + 1) % 1000 == 0 or episode == 0:
                    imageio.mimsave(wdir + str(episode+1) + '/' + str(gamma) + '_' + str(epsilon) + '_movie.gif', images, duration=0.05)

                episode_rewards[episode, idx] = episode_reward
                episode_iterations[episode, idx] = it
            np.savetxt(wdir + str(gamma) + "_" + str(idx) + "_Q.txt", Q, '%.4f', delimiter=',')
            # Close environment
            env.close()

        np.savetxt(wdir + str(gamma) + "_episode_rewards.csv", episode_rewards, '%i', delimiter=',')
        np.savetxt(wdir + str(gamma) + "_episode_iterations.csv", episode_iterations, '%i', delimiter=',')
        np.savetxt(wdir + str(gamma) + "_episode_distance.csv", episode_distance, '%i', delimiter=',')
        np.savetxt(wdir + str(gamma) + "_actions.csv", np.array([actions_0, actions_1, actions_2, actions_3]), '%i', delimiter=',')
        print("Done")

"""
Ownership of the following code developed as a result of assigned institutional effort,
an assignment of the CS 7641 Machine Learning course shall reside with GT and the instructors
of this class. If the document is released into the public domain it will violate the GT Honor Code.
"""