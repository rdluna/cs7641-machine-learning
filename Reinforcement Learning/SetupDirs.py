import os

wdir = "D:/Users/212415648/Documents/EDUCATION/Masters/Machine Learning/Assignment 4/"
names = ["4x4", "8x8"]
dirs = ["/Policy Iteration", "/Value Iteration", "/Q-Learning"]
learners = ["/greedy", "/non-greedy"]

for name in names:
    for learner in learners:
        for dir_ in dirs:
            mk_name = wdir + name + dir_
            if dir_ == "/Q-Learning":
                mk_name = wdir + name + dir_ + learner
            if not os.path.exists(mk_name):
                os.makedirs(mk_name)