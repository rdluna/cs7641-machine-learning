DISCLAIMER: The code associated with this readme, as well as the analysis contained is for sole use for evaluation of Georgia Tech's OMSCS CS7641 course.
The contents of this assignment should not be reproduced or copied anywhere, they shall only reside in the private repository and with the instructors.
The contents of the repository may be deleted at any moment by the instructors of the course (after grading).

This assignment uses:
1) MATLAB r2017a (full version, provided by Georgia Tech)
2) Anaconda with Python 3.6.2
3) RStudio 1.0.153 with R version 3.4.1

The code folder has 3 folders:
abalone: contains Python code to run k-means, expectation maximization and all feature selection algorithms for the abalone dataset
adult: the same for the abalone dataset
ANN: code to train the ANN with Matlab for the adult dataset

As there were a lot of files in the directory, making it go into excess of 2GB, the complete directories, with code and data are in GitLab:
https://gitlab.com/delunalara/assignment3
That is a private repository. To request access email me at rodrigo.deluna@bhge.com or rodrigo.deluna@gatech.edu

The Python files to run are:
1) adult/abalone: contains the code to train all the algorithms
2) clustering_selected: on each directory (abalone/adult) this file runs k-means on the transformed datasets
3) gmm_selected: on each directory (abalone/adult) this file runs expectation maximization on the transformed datasets

In the ANN folder there are 7 directories, each contains a Matlab script called train_nnet
The architecture is fixed to 10 neurons, 1 hidden layer and the learning rate and train/test/validation splits are optimized for each dataset.
These folders are:
1) nnet2: training on the original dataset
2) pca: training on PCA data
3) ica: traning on ICA data
4) rca: training on RCA data
5) tree: training on tree-selected data
6) kmanes: traning with kmeans features
7) gmm: traning with expectation maximization features

The plots and some minor processing are made with ggplot, the code for them is the rll8-analysis.Rmd file.