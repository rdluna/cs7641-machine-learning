import numpy as np
import pandas as pd
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score
from sklearn import metrics
from sklearn.preprocessing import MinMaxScaler

data_train = pd.read_csv('C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\adult.train', dtype='category')
data_test = pd.read_csv('C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\adult.test', dtype='category')

names = ['age', 'workclass', 'fnlgwt', 'education', 'education_num', 'marital_status', 'occupation', 'relationship',
         'race', 'sex', 'capital_gain', 'capital_loss', 'hrs_week', 'native_country', 'K']

data_train.columns = names
data_test.columns = names

data = pd.concat([data_train, data_test])

data.K[data.K == ' <=50K.'] = ' <=50K'
data.K[data.K == ' >50K.'] = ' >50K'
data.K = data.K.astype('category')
data.native_country = data.native_country.astype('category')
data.education_num = data.education_num.astype('int64')

cat_columns = data.select_dtypes(['category']).columns
for column in cat_columns:
    dummies = pd.get_dummies(data[column])
    data = pd.concat([data, dummies], axis=1)
    data.drop(column, axis=1, inplace=True)

data = data[data.columns[data.columns.values != ' ']]
income = data[data.columns.values[-2:]]
data = data[data.columns.values[0:-2]]

data = data.values
income = income.values

data = MinMaxScaler(feature_range=(0, 1)).fit_transform(data)
income = MinMaxScaler(feature_range=(0, 1)).fit_transform(income)

pca_data = pd.read_csv('C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\pca_data_sel.csv', header=None)
ica_data = pd.read_csv('C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\ica_data_sel.csv', header=None)
rca_data = pd.read_csv('C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\rca_90.csv', header=None)
tree_data = pd.read_csv('C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\tree.csv', header=None)

print('Dimensions of PCA data: ' + str(pca_data.shape[1]))
print('Dimensions of ICA data: ' + str(ica_data.shape[1]))
print('Dimensions of RCA data (90% norm): ' + str(rca_data.shape[1]))
print('Dimensions of Tree data: ' + str(tree_data.shape[1]))

pca_labels = np.zeros((pca_data.shape[0], 19))
pca_silhouette_avg = np.zeros((19, 1))
pca_sse = np.zeros((19, 1))
pca_ari = np.zeros((19, 1))
pca_mis = np.zeros((19, 1))
pca_chs = np.zeros((19, 1))
pca_v_score = np.zeros((19, 3))

ica_labels = np.zeros((ica_data.shape[0], 19))
ica_silhouette_avg = np.zeros((19, 1))
ica_sse = np.zeros((19, 1))
ica_ari = np.zeros((19, 1))
ica_mis = np.zeros((19, 1))
ica_chs = np.zeros((19, 1))
ica_v_score = np.zeros((19, 3))

rca_labels = np.zeros((rca_data.shape[0], 19))
rca_silhouette_avg = np.zeros((19, 1))
rca_sse = np.zeros((19, 1))
rca_ari = np.zeros((19, 1))
rca_mis = np.zeros((19, 1))
rca_chs = np.zeros((19, 1))
rca_v_score = np.zeros((19, 3))

tree_labels = np.zeros((tree_data.shape[0], 19))
tree_silhouette_avg = np.zeros((19, 1))
tree_sse = np.zeros((19, 1))
tree_ari = np.zeros((19, 1))
tree_mis = np.zeros((19, 1))
tree_chs = np.zeros((19, 1))
tree_v_score = np.zeros((19, 3))
#
for idx, k in enumerate(np.arange(2, 21)):
    pca_clusterer = KMeans(n_clusters=k, random_state=1, n_init=10, algorithm='full')
    ica_clusterer = KMeans(n_clusters=k, random_state=1, n_init=10, algorithm='full')
    rca_clusterer = KMeans(n_clusters=k, random_state=1, n_init=10, algorithm='full')
    tree_clusterer = KMeans(n_clusters=k, random_state=1, n_init=10, algorithm='full')
    pca_clusterer = pca_clusterer.fit(pca_data)
    ica_clusterer = ica_clusterer.fit(ica_data)
    rca_clusterer = rca_clusterer.fit(rca_data)
    tree_clusterer = tree_clusterer.fit(tree_data)
    pca_labels[:, idx] = pca_clusterer.predict(pca_data)
    ica_labels[:, idx] = ica_clusterer.predict(ica_data)
    rca_labels[:, idx] = rca_clusterer.predict(rca_data)
    tree_labels[:, idx] = tree_clusterer.predict(tree_data)
    pca_trans = pca_clusterer.transform(pca_data)
    ica_trans = ica_clusterer.transform(ica_data)
    rca_trans = rca_clusterer.transform(rca_data)
    tree_trans = tree_clusterer.transform(tree_data)
    np.savetxt(X=pca_trans, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\clust\\pca_trans_' + str(k) + '.csv')
    np.savetxt(X=rca_trans, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\clust\\rca_trans_' + str(k) + '.csv')
    np.savetxt(X=ica_trans, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\clust\\ica_trans_' + str(k) + '.csv')
    np.savetxt(X=tree_trans, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\clust\\tree_trans_' + str(k) + '.csv')
    pca_sse[idx, 0] = pca_clusterer.score(pca_data)
    ica_sse[idx, 0] = ica_clusterer.score(ica_data)
    rca_sse[idx, 0] = rca_clusterer.score(rca_data)
    tree_sse[idx, 0] = tree_clusterer.score(tree_data)
    pca_mis[idx, 0] = metrics.adjusted_mutual_info_score(income[:, 1], pca_labels[:, idx])
    ica_mis[idx, 0] = metrics.adjusted_mutual_info_score(income[:, 1], ica_labels[:, idx])
    rca_mis[idx, 0] = metrics.adjusted_mutual_info_score(income[:, 1], rca_labels[:, idx])
    tree_mis[idx, 0] = metrics.adjusted_mutual_info_score(income[:, 1], tree_labels[:, idx])
    pca_ari[idx, 0] = metrics.adjusted_rand_score(income[:, 1], pca_labels[:, idx])
    rca_ari[idx, 0] = metrics.adjusted_rand_score(income[:, 1], rca_labels[:, idx])
    ica_ari[idx, 0] = metrics.adjusted_rand_score(income[:, 1], ica_labels[:, idx])
    tree_ari[idx, 0] = metrics.adjusted_rand_score(income[:, 1], tree_labels[:, idx])
    pca_chs[idx, 0] = metrics.calinski_harabaz_score(data, pca_labels[:, idx])
    ica_chs[idx, 0] = metrics.calinski_harabaz_score(data, ica_labels[:, idx])
    rca_chs[idx, 0] = metrics.calinski_harabaz_score(data, rca_labels[:, idx])
    tree_chs[idx, 0] = metrics.calinski_harabaz_score(data, tree_labels[:, idx])
    pca_silhouette_avg[idx, 0] = silhouette_score(pca_data, pca_labels[:, idx], sample_size=12000, random_state=10)
    ica_silhouette_avg[idx, 0] = silhouette_score(ica_data, ica_labels[:, idx], sample_size=12000, random_state=10)
    rca_silhouette_avg[idx, 0] = silhouette_score(rca_data, rca_labels[:, idx], sample_size=12000, random_state=10)
    tree_silhouette_avg[idx, 0] = silhouette_score(tree_data, tree_labels[:, idx], sample_size=12000, random_state=10)
    pca_v_score[idx, :] = metrics.homogeneity_completeness_v_measure(income[:, 1], pca_labels[:, idx])
    ica_v_score[idx, :] = metrics.homogeneity_completeness_v_measure(income[:, 1], ica_labels[:, idx])
    rca_v_score[idx, :] = metrics.homogeneity_completeness_v_measure(income[:, 1], rca_labels[:, idx])
    tree_v_score[idx, :] = metrics.homogeneity_completeness_v_measure(income[:, 1], tree_labels[:, idx])
    print("k = " + str(k) + ' DONE')

silhouette_avg = np.column_stack((pca_silhouette_avg, ica_silhouette_avg, rca_silhouette_avg, tree_silhouette_avg))
np.savetxt(X=silhouette_avg, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\silhouette_sel.csv', delimiter=',', header='pca,ica,rca,tree')
v_score = np.column_stack((pca_v_score, ica_v_score, rca_v_score, tree_v_score))
np.savetxt(X=v_score, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\vscore_sel.csv', delimiter=',')
sse = np.column_stack((pca_sse, ica_sse, rca_sse, tree_sse))
np.savetxt(X=sse, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\sse_sel.csv', delimiter=',')
ari = np.column_stack((pca_ari, rca_ari, ica_ari, tree_ari))
np.savetxt(X=ari, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\ari_sel.csv', delimiter=',')
mis = np.column_stack((pca_mis, ica_mis, rca_mis, tree_mis))
np.savetxt(X=mis, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\mis_sel.csv', delimiter=',')
chs = np.column_stack((pca_chs, ica_chs, rca_chs, tree_chs))
np.savetxt(X=chs, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\chs_sel.csv', delimiter=',')
np.savetxt(X=pca_labels, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\pca_labels_sel.csv', delimiter=',')
np.savetxt(X=ica_labels, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\ica_labels_sel.csv', delimiter=',')
np.savetxt(X=rca_labels, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\rca_labels_sel.csv', delimiter=',')
np.savetxt(X=tree_labels, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\tree_labels_sel.csv', delimiter=',')