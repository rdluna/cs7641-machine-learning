import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from sklearn.mixture import GaussianMixture
from sklearn.metrics import silhouette_score
from sklearn.decomposition import PCA, FastICA
from sklearn.preprocessing import MinMaxScaler
from sklearn.random_projection import GaussianRandomProjection
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.feature_selection import SelectFromModel
from scipy.stats import kurtosis
from sklearn import metrics

data_train = pd.read_csv('adult.train', dtype='category')
data_test = pd.read_csv('adult.test', dtype='category')

names = ['age', 'workclass', 'fnlgwt', 'education', 'education_num', 'marital_status', 'occupation', 'relationship',
         'race', 'sex', 'capital_gain', 'capital_loss', 'hrs_week', 'native_country', 'K']

data_train.columns = names
data_test.columns = names

data = pd.concat([data_train, data_test])

data.K[data.K == ' <=50K.'] = ' <=50K'
data.K[data.K == ' >50K.'] = ' >50K'
data.K = data.K.astype('category')
data.native_country = data.native_country.astype('category')
data.education_num = data.education_num.astype('int64')

cat_columns = data.select_dtypes(['category']).columns
for column in cat_columns:
    dummies = pd.get_dummies(data[column])
    data = pd.concat([data, dummies], axis=1)
    data.drop(column, axis=1, inplace=True)

data = data[data.columns[data.columns.values != ' ']]
income = data[data.columns.values[-2:]]
data = data[data.columns.values[0:-2]]

names = data.columns.values
pd.DataFrame(names).to_csv("C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\names.csv")
data = data.values
income = income.values

data = MinMaxScaler(feature_range=(0, 1)).fit_transform(data)
income = MinMaxScaler(feature_range=(0, 1)).fit_transform(income)

labels = np.zeros((data.shape[0], 19))
silhouette_avg = np.zeros((19, 1))
sse = np.zeros((19, 1))
ari = np.zeros((19, 1))
mis = np.zeros((19, 1))
chs = np.zeros((19, 1))
v_score = np.zeros((19, 3))

for idx, k in enumerate(np.arange(2, 21)):
    clusterer = KMeans(n_clusters=k, random_state=1, n_init=20, algorithm='full')
    clusterer = clusterer.fit(data)
    labels[:, idx] = clusterer.predict(data)
    silhouette_avg[idx, 0] = silhouette_score(data, labels[:, idx], sample_size=12000, random_state=10)
    sse[idx, 0] = clusterer.score(data)
    mis[idx, 0] = metrics.adjusted_mutual_info_score(income[:, 1], labels[:, idx])
    ari[idx, 0] = metrics.adjusted_rand_score(income[:, 1], labels[:, idx])
    chs[idx, 0] = metrics.calinski_harabaz_score(data, labels[:, idx])
    v_score[idx, :] = metrics.homogeneity_completeness_v_measure(income[:, 1], labels[:, idx])
    data_trans = clusterer.transform(data)
    np.savetxt(X=data_trans, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\clust0\\pca_trans_' + str(k) + '.csv')
    print('For k = ' + str(k) + ', mean silhouette: ' + str(silhouette_avg[idx, 0]))
    print('SSE = ' + str(sse[idx, 0]))
    print('MIS = ' + str(mis[idx, 0]))
    print('ARI = ' + str(ari[idx, 0]))
    print('CHS = ' + str(chs[idx, 0]))
    print('V score = ' + str(v_score[idx, :]))

np.savetxt(X=labels, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\kmeans_labels.csv', delimiter=',')
np.savetxt(X=silhouette_avg, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\mean_silhouette.csv', delimiter=',', header='mean_silhouette')
np.savetxt(X=v_score, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\kmeans_vscore.csv', delimiter=',')
np.savetxt(X=sse, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\kmeans_sse.csv', delimiter=',')
np.savetxt(X=mis, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\kmeans_mis.csv', delimiter=',')
np.savetxt(X=ari, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\kmeans_ari.csv', delimiter=',')
np.savetxt(X=chs, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\kmeans_chs.csv', delimiter=',')


lowest_bic = np.infty
n_components_range = np.arange(2, 21)
cv_types = ['spherical', 'tied', 'diag', 'full']
bic = np.zeros((len(cv_types), len(n_components_range)))
aic = np.zeros((len(cv_types), len(n_components_range)))
gmm_labels = np.zeros((len(cv_types), len(n_components_range)))
for i, cv_type in enumerate(cv_types):
    for j, n_components in enumerate(n_components_range):
        # Fit a Gaussian mixture with EM
        gmm = GaussianMixture(n_components=n_components, covariance_type=cv_type,
                              n_init=10, random_state=10)
        gmm.fit(data)
        gmm_labels = gmm.predict(data)
        bic[i, j] = gmm.bic(data)
        aic[i, j] = gmm.aic(data)
        gmm_proba = gmm.predict_proba(data)
        gmm_score_samples = gmm.score_samples(data)
        print('For ' + str(n_components) + ' components with ' + cv_type +
              ' cv, AIC = ' + str(aic[i, j]) +
              ' BIC = ' + str(bic[i, j]))
        if bic[i, j] < lowest_bic:
            lowest_bic = bic[i, j]
            best_gmm = gmm
            best_labels = gmm_labels
            best_proba = gmm_proba
            best_score_samples = gmm_score_samples

np.savetxt(X=bic, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\bic_gmm.csv', delimiter=',', header='bic')
np.savetxt(X=aic, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\aic_gmm.csv', delimiter=',', header='aic')
np.savetxt(X=best_labels, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\gmm_labels.csv', delimiter=',')
best_features = np.column_stack((best_labels, best_proba, best_score_samples))
np.savetxt(X=best_features, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\gmm_features.csv', delimiter=',', header='labels,proba,score_samples')

pca = PCA(random_state=10).fit(data)
np.savetxt(X=np.column_stack((pca.explained_variance_ratio_,pca.explained_variance_ratio_.cumsum())), fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\pca.csv', delimiter=',', header='explained,cumsum')
pca_data = pca.transform(data)
pca_data_sel = pca_data[:,0:np.argmax(pca.explained_variance_ratio_.cumsum() >= 0.95)]
np.savetxt(X=pca_data, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\pca_data.csv', delimiter=',')
np.savetxt(X=pca_data_sel, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\pca_data_sel.csv', delimiter=',')

ica = FastICA(random_state=10).fit(data)
ica_data = ica.transform(data)
ica_kurtosis = kurtosis(ica_data)
np.savetxt(X=ica_data, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\ica_data.csv', delimiter=',')
np.savetxt(X=ica_kurtosis, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\ica_kurtosis.csv', delimiter=',', header='kurtosis')
ica_data_sel = ica_data[:,ica_kurtosis >= np.median(ica_kurtosis)]
np.savetxt(X=ica_data_sel, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\ica_data_sel.csv', delimiter=',')

fro_data = np.linalg.norm(data, 'fro')
print('Norm of data = ', fro_data)
fro_norm = np.zeros((data.shape[1], 10))
for idx, n_components in enumerate(np.arange(data.shape[1])):
    for i in np.arange(10):
        rca = GaussianRandomProjection(n_components=n_components+1, random_state=i).fit(data)
        rca_data = rca.transform(data)
        reconstructed_data = np.dot(rca_data, np.transpose(np.linalg.pinv(rca.components_)))
        fro_norm[idx, i] = np.linalg.norm(reconstructed_data, 'fro')
        print('For ' + str(n_components+1) + ' components, norm = ' + str(fro_norm[idx, i]))
min_c = np.zeros(10)
for i in np.arange(10):
   min_c[i] = (np.argmax(fro_norm[:, i] >= fro_data*0.90))
rca_90 = GaussianRandomProjection(n_components=int(min(min_c)), random_state=int(np.argmin(min_c))).fit_transform(data)
np.savetxt(X=rca_90, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\rca_90.csv', delimiter=',')
np.savetxt(X=fro_norm, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\rca_norm.csv', delimiter=',')

ctree = ExtraTreesClassifier(random_state=10)
ctree = ctree.fit(data, income[:, 1])
np.savetxt(X=ctree.feature_importances_, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\tree_features.csv', delimiter=',')
model = SelectFromModel(ctree, prefit=True, threshold='mean')
tree_select = model.transform(data)
names_tree = names[model.get_support()]
pd.DataFrame(names_tree).to_csv('C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\tree_names.csv')
np.savetxt(X=tree_select, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\adult\\tree.csv', delimiter=',')