setwd('C:/Users/rodri/PycharmProjects/Assignment 3/')
data = read.csv('adult_kmeans.csv')
silh = read.csv('mean_silhouette.csv')
colnames(silh)=c("mean_silh")
require(ggplot2)

plot1 = ggplot(data,aes(x=education_num,
                y=capital_gain,
                color=as.factor(relationship))) +
  geom_jitter(width = 0.1,alpha=0.75) + 
  theme_bw() + facet_grid(sex ~ labels, 
                          labeller = labeller(sex = c("0"="Men","1"="Women"),
                                              labels=c("0"="Cluster 1","1"="Cluster 2"))) + 
  scale_color_brewer(palette="Paired") +
  labs(x="Education (yrs)",y="Capital Gain",color="Relationship")

plot2 = ggplot(data,aes(x=education_num,
                y=capital_gain,
                color=age)) +
  geom_jitter(width = 0.1,alpha=0.75) + 
  theme_bw() + facet_grid(sex ~ labels, 
                          labeller = labeller(sex = c("0"="Men","1"="Women"),
                                              labels=c("0"="Cluster 1","1"="Cluster 2"))) + 
  scale_color_distiller(palette="Spectral",limits=c(20,80)) +
  labs(x="Education (yrs)",y="Capital Gain",color="Age")

plot3 = ggplot(data,aes(x=education_num,
                y=capital_gain,
                color=as.factor(race))) +
  geom_jitter(width = 0.1,alpha=0.75) + 
  theme_bw() + facet_grid(sex ~ labels, 
                          labeller = labeller(sex = c("0"="Men","1"="Women"),
                                              labels=c("0"="Cluster 1","1"="Cluster 2"))) + 
  scale_color_brewer(palette="Paired") +
  labs(x="Education (yrs)",y="Capital Gain",color="Race")

print(plot1)
print(plot2)
print(plot3)