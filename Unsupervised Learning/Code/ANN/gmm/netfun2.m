function testval = netfun2(XTRAIN, YTRAIN, XTEST, YTEST, train_split, test_split, val_split, lr)

net = patternnet(10,'traingd','crossentropy');
net.trainParam.time = 300;
net.trainParam.max_fail = 10;
net.trainParam.showWindow = 0;
net.trainParam.showCommandLine = 1;
net.trainParam.lr = lr;
net.divideParam.trainRatio = train_split;
net.divideParam.testRatio = test_split;
net.divideParam.valRatio = val_split;
net.trainParam.epochs = 99e99;
net = configure(net, XTRAIN', YTRAIN');

net = train(net, XTRAIN', YTRAIN','useParallel','yes');
yNet = net(XTEST');
cp = crossentropy(net,YTEST', yNet);
testval = cp;

end