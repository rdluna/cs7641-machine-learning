clc
clear
close force all

data = readtable('data_init.csv');
labels = readtable('kmeans_labels.csv');
labels = labels{:,1};
clust_distances = readtable('trans_2.csv');
data.labels = labels;
data.clustdist1 = clust_distances{:,1};
data.clustdist2 = clust_distances{:,2};

income = readtable('income_init.csv');
income.Properties.VariableNames = {'Out1','Out2'};
data = horzcat(data,income);

rng(0112358)
[train_idx,test_idx] = dividerand(size(data,1),0.70,0.30);
data_train = data(train_idx,:);
data_test = data(test_idx,:);

train_split = 0.90:-0.10:0.10;
test_split  = 0.10:0.10:0.90;

%% Neural networks - Part 0
data_train_nn = data_train;
data_test_nn = data_test;
inputs_train_nn = table2array(data_train_nn(:,1:end-2));
output_train_nn = table2array(data_train_nn(:,end-1:end));
inputs_test_nn = table2array(data_test_nn(:,1:end-2));
output_test_nn = table2array(data_test_nn(:,end-1:end));

%% Neural networks - Part 1
tic
n_neurons = 10;
n_layers = 1;
learning_rate = (0.1:0.1:0.9);

% Training a classification network with standard gradient descent by
% optimizing learning rate
net_crossv_error = zeros(numel(n_layers),numel(n_neurons));

for j = 1:numel(learning_rate)
    net_crossv_error(j) = mean(crossval(@(XTRAIN, YTRAIN, XTEST, YTEST)netfun(XTRAIN, YTRAIN, XTEST, YTEST, n_neurons, n_layers, 1000, learning_rate(j), 'traingd'),...
        inputs_train_nn, output_train_nn, 'KFold',5));
end
[~,net_min_cv] = min(net_crossv_error);
net_best_lr = learning_rate(net_min_cv);

save('net_crossv_error','net_crossv_error')

figure
clf
plot(learning_rate,net_crossv_error,'-kd')
grid on
box on
xlabel('Gradient descent learning rate')
ylabel('Crossvalidation cross entropy')
set(gcf,'Color','w')
saveas(gcf,'nnet_train1.png')

%% Neural networks - Part 2
% Check if changing training/validation/test splits affects the
% crossvalidation error
net_crossv_error_split = zeros(numel(n_layers),numel(n_neurons));

net_splits = [0.80 0.10 0.10;...
              0.60 0.20 0.20;...
              0.50 0.25 0.25;...
              0.70 0.20 0.10;...
              0.60 0.30 0.10;...
              0.50 0.30 0.20;...
              0.60 0.10 0.30;...
              0.50 0.20 0.30];

for i = 1:size(net_splits,1)
    net_crossv_error_split(i) = mean(crossval(@(XTRAIN, YTRAIN, XTEST, YTEST)netfun2(XTRAIN, YTRAIN, XTEST, YTEST, net_splits(i,1), net_splits(i,2), net_splits(i,3), net_best_lr),...
        inputs_train_nn, output_train_nn, 'KFold',5));
end

save('net_crossv_error_split','net_crossv_error_split')

net_splits_table = array2table(sortrows([net_crossv_error_split' net_splits]),'VariableNames',{'train','test','validation','cverror'});

figure
plot(net_crossv_error_split,'-kd')
grid on
box on
xlabel('Split')
ylabel('Crossvalidation cross entropy')
set(gcf,'Color','w')
saveas(gcf,'nnet_train2.png')

figure
subplot(3,1,1)
scatter(net_splits(:,1),net_crossv_error_split,'kd')
xlim([0 1])
xlabel('Training split')
subplot(3,1,2)
scatter(net_splits(:,2),net_crossv_error_split,'kd')
xlim([0 1])
xlabel('Test split')
ylabel('Crossvalidation cross entropy')
subplot(3,1,3)
scatter(net_splits(:,3),net_crossv_error_split,'kd')
xlim([0 1])
xlabel('Validation split')
set(gcf,'Color','w')
saveas(gcf,'nnet_train3.png');
etime = toc;
%% Neural Network Learning Curve
lc_net_test_error = zeros(numel(train_split),1);
lc_net_train_error = zeros(numel(train_split),1);
lc_net_perf = cell(numel(train_split),1);

for ii = 1:numel(train_split)
    rng(ii*2)
    [lc_train_idx,lc_test_idx] = dividerand(size(data,1),train_split(ii),test_split(ii));
    lc_train = data(lc_train_idx,:);
    lc_test = data(lc_test_idx,:);
    inputs_train_lc = table2array(lc_train(:,1:end-2));
    output_train_lc = table2array(lc_train(:,end-1:end));
    inputs_test_lc = table2array(lc_test(:,1:end-2));
    output_test_lc = table2array(lc_test(:,end-1:end));
    [lc_net,lc_net_train_error(ii)] = netfun3(inputs_train_lc, output_train_lc, inputs_test_lc, output_test_lc, net_splits_table{1,2}, net_splits_table{1,3}, net_splits_table{1,4}, net_best_lr);
    lc_net_test = lc_net(inputs_test_lc');
    cp_test = crossentropy(lc_net,output_test_lc', lc_net_test);
    lc_net_test_error(ii) = cp_test;
end

save('lc_net_test_error','lc_net_test_error')
save('lc_net_train_error','lc_net_train_error')

figure
hold on
plot(train_split,lc_net_test_error,'-bd')
plot(train_split,lc_net_train_error,'-gd')
grid on
box on
xlabel('Training split')
ylabel('Cross entropy')
set(gcf,'Color','w')
legend('Test','Train','location','best')
title({['Train CE at 70% split: ' num2str(lc_net_train_error(train_split == 0.70)) ],['Test CE at 70% split: ', num2str(lc_net_test_error(train_split == 0.70))]})
set(findall(gcf,'-property','FontSize'),'FontSize',16)
saveas(gcf,'nnet_lc.png');
save workspace_nnet