import numpy as np
import pandas as pd
from sklearn.mixture import GaussianMixture
from sklearn.preprocessing import MinMaxScaler

data = pd.read_csv('abalone.data', header=None)
data.columns = ['sex', 'length', 'diameter', 'height', 'whole_weight', 'shucked_weight', 'viscera_weight', 'shell_weight', 'rings']
data.sex = data.sex.astype('category')
data.sex = data.sex.cat.codes
class_ = data.sex.values
data.drop('sex', axis=1, inplace=True)
names = data.columns.values

data = data.values

data = MinMaxScaler(feature_range=(0, 1)).fit_transform(data)
pd.DataFrame(data, columns=names).to_csv('C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\abalone\\data_init.csv',index=False)
pd.DataFrame(class_, columns=["class"]).to_csv('C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\abalone\\class_init.csv',index=False)

pca_data = pd.read_csv('C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\abalone\\pca_data_sel.csv', header=None)
ica_data = pd.read_csv('C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\abalone\\ica_data_sel.csv', header=None)
rca_data = pd.read_csv('C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\abalone\\rca_90.csv', header=None)
tree_data = pd.read_csv('C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\abalone\\tree.csv', header=None)

print('Dimensions of PCA data: ' + str(pca_data.shape[1]))
print('Dimensions of ICA data: ' + str(ica_data.shape[1]))
print('Dimensions of RCA data (90% norm): ' + str(rca_data.shape[1]))
print('Dimensions of Tree data: ' + str(tree_data.shape[1]))

algs = ['pca', 'ica', 'rca', 'tree']
for alg in algs:
    if alg == 'pca':
        data = pca_data
    if alg == 'ica':
        data = ica_data
    if alg == 'rca':
        data = rca_data
    if alg == 'tree':
        data = tree_data
    lowest_bic = np.infty
    n_components_range = np.arange(2, 21)
    cv_types = ['spherical', 'tied', 'diag', 'full']
    bic = np.zeros((len(cv_types), len(n_components_range)))
    aic = np.zeros((len(cv_types), len(n_components_range)))
    gmm_labels = np.zeros((len(cv_types), len(n_components_range)))
    for i, cv_type in enumerate(cv_types):
        for j, n_components in enumerate(n_components_range):
            # Fit a Gaussian mixture with EM
            gmm = GaussianMixture(n_components=n_components, covariance_type=cv_type,
                                  n_init=10, random_state=10)
            gmm.fit(data)
            gmm_labels = gmm.predict(data)
            gmm_proba = gmm.predict_proba(data)
            gmm_score_samples = gmm.score_samples(data)
            bic[i, j] = gmm.bic(data)
            aic[i, j] = gmm.aic(data)
            print('For ' + str(n_components) + ' components with ' + cv_type +
                  ' cv, AIC = ' + str(aic[i, j]) +
                  ' BIC = ' + str(bic[i, j]))
            if bic[i, j] < lowest_bic:
                lowest_bic = bic[i, j]
                best_gmm = gmm
                best_labels = gmm_labels
                best_proba = gmm_proba
                best_score_samples = gmm_score_samples
    best_features = np.column_stack((best_labels, best_proba, best_score_samples))
    np.savetxt(X=bic, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\abalone\\' + alg + '_bic_gmm.csv', delimiter=',', header='bic')
    np.savetxt(X=aic, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\abalone\\' +alg + '_aic_gmm.csv', delimiter=',', header='aic')
    np.savetxt(X=best_features, fname='C:\\Users\\rodri\\PycharmProjects\\Assignment 3\\abalone\\' +alg + '_gmm_features.csv', delimiter=',', header='labels,proba,score_samples')